<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//User side routes
//Site main pages
Route::get('/', 'HomeController@index')->name('home');
Route::get('/most-popular', 'HomeController@viewPopular')->name('most_popular');
Route::get('/most-popular/{keyPhone}', 'HomeController@viewKeyProducts')
    ->name('key_product_view')
    ->where('keyPhone', '[a-z]+')
    ->middleware('key.product');
Route::get('/best-prices', 'HomeController@viewBestPrices')->name('best_prices_view');
Route::get('/about', 'HomeController@viewAbout')->name('about_view');

//Language switching
Route::get('/lang/{lang}', 'LanguageController@switchLang')->name('lang_switch')->where('lang', '[a-z]+');

//Search section
Route::any('/search', 'SearchController@search')->name('search');
Route::post('/search/autocomplete', 'SearchController@autocomplete')->name('search_autocomplete');

//Product section
Route::get('/sell/{product}', 'ProductController@view')
    ->where('product', '[0-9a-zA-Z_-]+')
    ->name('product_view');

Route::get('/sell/{product}/filters/best-price', 'ProductController@filterBestPrice')
    ->where('product', '[0-9a-zA-Z_-]+')
    ->name('product_best_price_filtering')
    ->middleware('price.filter');
Route::get('/sell/{product}/filters/prices', 'ProductController@filterPrices')
    ->where('product', '[0-9a-zA-Z_-]+')
    ->name('product_prices_filtering')
    ->middleware('price.filter');


//Brand section
Route::get('/brands/{brand}', 'BrandController@view')->where('brand', '[0-9a-zA-Z_-]+')->name('brand_view');
Route::get('/brands/', 'BrandController@index')->name('brand_main');

//Sell link section
Route::get('/outlink/{link}', 'SellLinkController@redirect')->where('link', '[0-9]+')->name('sell_link');

//Recyclers Section
Route::get('/recyclers/', 'RecyclerController@index')->name('recyclers_list');
Route::get('/recyclers/{site}', 'RecyclerController@view')->where('site', '[0-9a-zA-Z_-]+')->name('recyclers_view');

//Sitemaps section
Route::get('/sitemaps/', 'SitemapsController@index')->name('sitemaps_main');
Route::get('/sitemaps/general', 'SitemapsController@general')->name('sitemaps_general');
Route::get('/sitemaps/products', 'SitemapsController@products')->name('sitemaps_products');
Route::get('/sitemaps/brands', 'SitemapsController@brands')->name('sitemaps_brands');

//Quiz section
Route::get('/quiz', 'QuizController@index')->name('quiz_main');
Route::get('/quiz/scenario/{answer}/{step}', 'QuizController@nextQuestion')
    ->where(['answer' => '[0-9]+', 'step' => '[0-9]+'])
    ->name('quiz_next_question');

// Admin section routes
Route::prefix('admin')->middleware('auth')->group(function () {

    Route::get('/', 'Admin\HomeController@index')->name('admin_main');

    //Site section
    Route::get('/sites', 'Admin\SiteController@index')->name('admin_sites_main');
    Route::get('/sites/view/{site}', 'Admin\SiteController@view')->where('site', '[0-9a-zA-Z_-]+')->name('admin_sites_view');
    Route::get('/sites/create', 'Admin\SiteController@create')->name('admin_sites_create');
    Route::get('/sites/delete/{site}', 'Admin\SiteController@destroy')->where('site', '[0-9a-zA-Z_-]+')->name('admin_sites_delete');
    Route::get('/sites/update/{site}', 'Admin\SiteController@update')->where('site', '[0-9a-zA-Z_-]+')->name('admin_sites_update');
    Route::post('/sites/save', 'Admin\SiteController@storage')->name('admin_sites_storage');

    //Brand section
    Route::get('/brands', 'Admin\BrandController@index')->name('admin_brands_view');
    Route::get('/brands/create', 'Admin\BrandController@create')->name('admin_brands_create');
    Route::get('/brands/delete/{brand}', 'Admin\BrandController@destroy')->where('brand', '[0-9a-zA-Z_-]+')->name('admin_brands_delete');
    Route::get('/brands/update/{brand}', 'Admin\BrandController@update')->where('brand', '[0-9a-zA-Z_-]+')->name('admin_brands_update');
    Route::post('/brands/save', 'Admin\BrandController@storage')->name('admin_brands_storage');

    //Payment section
    Route::get('/payments', 'Admin\PaymentController@index')->name('admin_payments_view');
    Route::get('/payments/{type}/create', 'Admin\PaymentController@create')
        ->name('admin_payments_create')
        ->where('type', '[a-z]+')
        ->middleware('payment.type');
    Route::get('/payments/{type}/update/{id}', 'Admin\PaymentController@update')
        ->where(['type' => '[a-z]+','id' => '[0-9]+'])
        ->name('admin_payments_update')
        ->middleware('payment.type');
    Route::post('/payments/{type}/save', 'Admin\PaymentController@storage')
        ->name('admin_payments_storage')
        ->where('type', '[a-z]+')
        ->middleware('payment.type');

    //ParseReport section
    Route::get('/reports/{site}/{type}/', 'Admin\ReportController@view')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_view')
        ->middleware('report.type');
    Route::get('/reports/{site}/{type}/delete/', 'Admin\ReportController@destroyByType')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_delete')
        ->middleware('report.type');
    Route::get('/reports/delete/', 'Admin\ReportController@destroyAll')->name('admin_reports_delete_all');
    Route::get('/report/{report}/delete/{type}', 'Admin\ReportController@destroy')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_delete_single')
        ->middleware('report.type.delete');

    //Products section
    Route::get('/search', 'Admin\SearchController@index')->name('admin_search');
    Route::any('/search/result', 'Admin\SearchController@search')->name('admin_search_result');
    Route::post('/search/autocomplete', 'Admin\SearchController@autocomplete')->name('admin_search_autocomplete');
    Route::get('/product/{product}', 'Admin\ProductController@view')->name('admin_product_view')->where('product', '[0-9a-zA-Z_-]+');
    Route::post('/product/{product}/store', 'Admin\ProductController@store')->name('admin_product_store')->where('product', '[0-9a-zA-Z_-]+');
    Route::get('/no-image', 'Admin\ProductController@viewNoImage')->name('admin_product_no_image');
    Route::get('/most-popular', 'Admin\PopularProductController@index')->name('admin_popular_main');
    Route::get('/most-popular/{product}', 'Admin\PopularProductController@make')->name('admin_make_popular')->where('product', '[0-9a-zA-Z_-]+');
    Route::get('/most-popular/{product}/remove', 'Admin\PopularProductController@remove')->name('admin_remove_popular')->where('product', '[0-9a-zA-Z_-]+');

    //Description section
    Route::get('/descriptions/view/{type}/{name}', 'Admin\DescriptionController@view')
        ->name('admin_descriptions_view')
        ->where(['type' => '[a-z]+', 'name' => '[0-9a-zA-Z_-]+'])
        ->middleware('description.type');
    Route::post('/descriptions/store/{type}', 'Admin\DescriptionController@store')
        ->name('admin_descriptions_store')
        ->where('type', '[a-z]+')
        ->middleware('description.type');
    Route::get('/descriptions/delete/{description}', 'Admin\DescriptionController@destroy')
        ->name('admin_descriptions_delete')
        ->where('description', '[0-9]+');
    Route::get('/descriptions/create/{type}/{model}', 'Admin\DescriptionController@create')
        ->name('admin_descriptions_create')
        ->where(['type' => '[a-z]+', 'model' => '[0-9]+'])
        ->middleware('description.type');

    //Quiz section
    Route::get('/quiz', 'Admin\QuizController@index')->name('admin_quiz_main');
    Route::post('/quiz/condition/store', 'Admin\ConditionController@store')->name('admin_condition_weight_store');
    Route::post('/quiz/question/order/store', 'Admin\QuizController@storeQuestionOrder')
        ->name('admin_question_order_store');

    //Quiz questions subsection
    Route::get('/question/create', 'Admin\QuestionController@create')->name('admin_question_create');
    Route::get('/question/update/{question}', 'Admin\QuestionController@update')
        ->where('question', '[0-9]+')
        ->name('admin_question_update');
    Route::get('/question/delete/{question}', 'Admin\QuestionController@delete')
        ->where('question', '[0-9]+')
        ->name('admin_question_delete');
    Route::post('/question/store', 'Admin\QuestionController@store')->name('admin_question_store');

    //Quiz questions translations subsection
    Route::get('/quiz/question/translation/view/{question}', 'Admin\QuestionTranslationController@index')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_view');
    Route::get('/quiz/question/translation/add/{question}', 'Admin\QuestionTranslationController@create')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_create');
    Route::get('/quiz/question/translation/update/{question}', 'Admin\QuestionTranslationController@update')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_update');
    Route::post('/quiz/question/translation/store', 'Admin\QuestionTranslationController@store')
        ->name('admin_question_translation_store');

    //Quiz answer subsection
    Route::any('/answer/delete/{answer}', 'Admin\AnswerController@delete')
        ->name('admin_answer_delete')
        ->where('answer', '[0-9]+');

    Route::get('/parse', 'Admin\HomeController@parse')->name('parse');

});
