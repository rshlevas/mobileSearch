<?php

namespace Tests\Unit;

use App\Parsers\CSVParser\WebgainsParser\RecycleParser;
use App\Parsers\Parser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Traits\MethodInvoke;
use Tests\Unit\Traits\ParserMock;

class ParserTest extends TestCase
{
    use ParserMock, MethodInvoke;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var string
     */
    protected $parserClass = RecycleParser::class;

    /**
     * Set up test
     */
    public function setUp()
    {
        parent::setUp();
        $this->parser = $this->getParser($this->parserClass);
    }

    /**
     * Testing of parser filter function
     *
     * @dataProvider filterTestDataProvider
     *
     * @param $string
     * @param $result
     */
    public function test_filters($string, $result)
    {
        $filtered = $this->invokeMethod($this->parser, 'filter', [$string]);
        $this->assertEquals($result, $filtered);
    }

    /**
     * Data provider for test_filters
     *
     * @return array
     */
    public function filterTestDataProvider()
    {
        return [
            ['sony ericsson xperia', 'sony xperia'],
            ['ipad with retina display', 'ipad retina display'],
            ['(2007)', '2007'],
            ['+', ' '],
            ['Wi-Fi', 'WIFI'],
            ['WiFi', 'WIFI'],
            ['WI-FI', 'WIFI'],
            ['Wifi', 'WIFI'],
            ['wi-fi', 'WIFI'],
            ['32gb', '32GB'],
        ];
    }

    /**
     * Test of parser method of duplicates removing
     *
     * @dataProvider duplicatesDataProvider
     * @param $string
     * @param $result
     */
    public function test_remove_duplicates($string, $result)
    {
        $removeDuplicates = $this->invokeMethod($this->parser, 'removeDuplicates', [$string]);
        $this->assertEquals($result, $removeDuplicates);
    }

    /**
     * Data provider for test_remove_duplicates
     *
     * @return array
     */
    public function duplicatesDataProvider()
    {
        return [
            ['nokia nokia  nokia', 'nokia'],
            ['Nokia', 'Nokia'],
            ['nokiaF nokia nokia', 'nokiaF nokia'],
        ];
    }

    /**
     * Test of parser method for generating of product url name
     *
     * @dataProvider getUrlNameDataProvider
     * @param $name
     * @param $result
     */
    public function test_get_url_name($name, $result)
    {
        $nameParts = explode(' ', $name);
        $urlName = $this->invokeMethod($this->parser, 'getUrlName', [$nameParts]);
        $this->assertEquals($result, $urlName);
    }

    /**
     * Data Provider for test_get_url_name
     *
     * @return array
     */
    public function getUrlNameDataProvider()
    {
        return [
            ['Nokia Lumia B630', 'nokia_lumia_b630'],
            ['Apple Iphone X 32GB', 'apple_iphone_x_32gb'],
        ];
    }

    /**
     * Test of parser method for generating of product check name
     *
     * @dataProvider getCheckNameDataProvider
     * @param $name
     * @param $result
     */
    public function test_get_check_name($name, $result)
    {
        $nameParts = explode(' ', $name);
        $urlName = $this->invokeMethod($this->parser, 'getCheckName', [$nameParts]);
        $this->assertEquals($result, $urlName);
    }

    /**
     * Data Provider for test_get_check_name
     *
     * @return array
     */
    public function getCheckNameDataProvider()
    {
        return [
            ['Nokia Lumia B630', 'b630 lumia nokia'],
            ['Apple Iphone X 32GB', '32gb apple iphone x'],
            ['Iphone X 32GB Apple', '32gb apple iphone x'],
            ['Apple Iphone X plus 32GB', '32gb apple iphone plus x'],
            ['Apple Iphone X 32GB plus', '32gb apple iphone plus x'],
        ];
    }

    /**
     *
     * @dataProvider setProperDataDataProvider
     * @param $name
     * @param $result
     * @return void
     */
    public function test_set_proper_data($name, $result)
    {
        $data = ['product' => ['name' => $name]];
        $properData = $this->invokeMethod($this->parser, 'setProperData', [$data]);
        $this->assertEquals($result, $properData);
    }

    /**
     * Data Provider for test_set_proper_data
     *
     * @return array
     */
    public function setProperDataDataProvider()
    {
        $result = [
            "product" => [
                "name" => "Apple Iphone X WIFI 32GB",
                "check_name" => "32gb apple iphone wifi x",
                "url_name" => "apple_iphone_x_wifi_32gb",
                "model" => "Iphone X WIFI 32GB",
            ],
            "brand" => [
                "name" => "Apple",
                "url_name" => "apple"
            ]

        ];

        return [
            ['Apple Iphone X wi-fi 32gb', $result],
            ['Apple     Iphone X Wi-Fi    32 gb', $result],
            ['Apple .    Iphone X Wi-Fi, .   32 gb', $result],
            ['Apple Iphone X with wifi    32 gb', $result],
            ['Apple     Iphone   X   Wi-Fi    32Gb', $result],
        ];
    }
}
