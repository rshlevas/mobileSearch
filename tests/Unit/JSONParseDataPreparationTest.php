<?php

namespace Tests\Unit;

use App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser;
use App\Parsers\JSONParser\CustomParsers\FonebankParser;
use App\Parsers\JSONParser\CustomParsers\GeckoMobileParser;
use App\Parsers\JSONParser\CustomParsers\GiffgaffParser;
use App\Parsers\JSONParser\CustomParsers\MackbackParser;
use App\Parsers\JSONParser\CustomParsers\TescoMobileParser;
use App\Parsers\JSONParser\CustomParsers\TopDollarMobileParser;
use App\Parsers\JSONParser\CustomParsers\VodafoneParser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Traits\MethodInvoke;
use Tests\Unit\Traits\ParserMock;

class JSONParseDataPreparationTest extends TestCase
{
    use MethodInvoke, ParserMock;
    /**
     * A basic test example.
     *
     * @dataProvider prepareDataDataProvider
     * @return void
     */
    public function test_prepare_data($className, $line, $result)
    {
        $parser = $this->getParser($className);
        $preparedData = $this->invokeMethod($parser, 'prepareData', [$line]);
        $this->assertEquals($result, $preparedData);
    }

    public function prepareDataDataProvider()
    {
        /*
         * Base Webgains parser lines. Could be used for RecyclerParser
         */
        $carphoneWareHouse = [
            'brand' =>	'Iphone',
            'model'	=> 'SE 128GB',
            'product_name' =>	'APPLE iPhone SE 128GB',
            'url' => 'https://www.example.com',
            'prices' => [
                'Working' => [
                    'networks' => [
                        0 => [
                            'name' => 'Unlocked',
                            'price' =>	'120.00',
                        ],
                        1 => [
                            'name' => 'Vodafone',
                            'price' => '110.00',
                        ],
                    ],
                ],
                'Dead' => [
                    'price' => '25.00',
                ]
            ],
            'site' => "Carphonewarehouse",
            'updated_at' =>	"1518439066",
        ];

        $carPhoneResult = [
            "brand" => [
                "name" => "Apple"
            ],
            "product" => [
                "name" => "Apple iPhone SE 128GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "https://www.example.com",
            ],
            "price" => [
                0 => [
                    "price" => "120.00",
                    "currency" => "GBP",
                    "created_at" => '1518439066',
                    "condition" => 1,
                    "network" => 1,
                ],
                1 => [
                    "price" => "110.00",
                    "currency" => "GBP",
                    "created_at" => '1518439066',
                    "condition" => 1,
                    "network" => 3
                ],
                2 => [
                    "price" => "25.00",
                    "currency" => "GBP",
                    "created_at" => '1518439066',
                    "condition" => 2,
                    "network" => 1,
                ],
            ],

        ];

        return [
            [CarphoneWarehouseParser::class, $carphoneWareHouse, $carPhoneResult],
        ];
    }
}
