<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Parsers\CSVParser\AWINParser\O2RecycleParser;
use App\Parsers\CSVParser\WebgainsParser\EightMobileParser;
use App\Parsers\CSVParser\WebgainsParser\EnvirofoneParser;
use App\Parsers\CSVParser\WebgainsParser\MobileCashParser;
use App\Parsers\CSVParser\WebgainsParser\RecycleParser;
use App\Parsers\CSVParser\WebgainsParser\SimplyDropParser;
use App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser;
use App\Parsers\JSONParser\CustomParsers\FonebankParser;
use App\Parsers\JSONParser\CustomParsers\GeckoMobileParser;
use App\Parsers\JSONParser\CustomParsers\GiffgaffParser;
use App\Parsers\JSONParser\CustomParsers\MackbackParser;
use App\Parsers\JSONParser\CustomParsers\TescoMobileParser;
use App\Parsers\JSONParser\CustomParsers\TopDollarMobileParser;
use App\Parsers\JSONParser\CustomParsers\VodafoneParser;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Traits\MethodInvoke;
use Tests\Unit\Traits\ParserMock;

class CSVParseDataPreparationTest extends TestCase
{
    use MethodInvoke, ParserMock;
    /**
     * A basic test example.
     *
     * @dataProvider prepareDataDataProvider
     * @return void
     */
    public function test_prepare_data($className, $line, $result)
    {
        $parser = $this->getParser($className);
        $preparedData = $this->invokeMethod($parser, 'prepareData', [$line]);
        $this->assertEquals($result, $preparedData);
    }

    public function prepareDataDataProvider()
    {
        /*
         * Base Webgains parser lines. Could be used for RecyclerParser
         */
        $webgainsLine = [
            'brand' => 'Apple',
            'product_name' => 'Apple Iphone 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'www.fake?=https://example.com',
            'currency' => 'GBP',
            'price' => '20.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
            'image_thumbnail_url' => 'http;//image.thumb.url',
        ];

        $webgainsResult = [
            "brand" => [
                "name" => "Apple"
            ],
            "product" => [
                "name" => "Apple Iphone 6 32GB",
                "model" => "Iphone 6 32GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "https://example.com",
            ],
            "price" => [
                0 => [
                    "price" => "20.00",
                    "currency" => "GBP",
                    "created_at" => "2017-12-12",
                    "condition" => 1,
                    "network" => 1,
                ]
            ],
            "images" => [
                0 => [
                    "url" => "http;//image.url",
                    "product_class" => Product::class,
                ],
                1 => [
                    "url" => "http;//image.thumb.url",
                    "product_class" => Product::class,
                    "thumb" => true
                ]
            ]
        ];

        /*
          Envirofone parser data.
         */
        $envirofoneLine = [
            'brand' => 'Apple',
            'product_name' => 'Iphone 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'price' => '20.00',
            'non_working_price' => '0.00',
            'last_updated' => '2017-12-12',
            'image_large_url' => 'http;//image.url',
            'image_url' => 'http;//image.thumb.url',
        ];

        $envirofoneResult = [
            "brand" => [
                "name" => "Apple"
            ],
            "product" => [
                "name" => "Apple Iphone 6 32GB",
                "model" => "Iphone 6 32GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "http://example.com",
            ],
            "price" => [
                0 => [
                    "price" => "20.00",
                    "currency" => "GBP",
                    "created_at" => "2017-12-12",
                    "condition" => 1,
                    "network" => 1,
                ],
                1 => [
                    'price' => '0.00',
                    'currency' => 'GBP',
                    'created_at' => "2017-12-12",
                    'condition' => 2,
                    'network' => 1,
                ],
            ],
            "images" => [
                0 => [
                    "url" => "http;//image.thumb.url",
                    "product_class" => Product::class,
                    "thumb" => true
                ],
                1 => [
                    "url" => "http;//image.url",
                    "product_class" => Product::class,
                ]
            ]
        ];

        /*
         * MobileCashParser
         */
        $mobileCashLines = [
            'product_name' => 'Apple Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'price' => '20.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
        ];

        $mobileCashResult = [
            "product" => [
                "name" => "Apple Iphone 6 32GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "http://example.com",
            ],
            "price" => [
                0 => [
                    "price" => "20.00",
                    "currency" => "GBP",
                    "created_at" => "2017-12-12",
                    "condition" => 1,
                    "network" => 1,
                ]
            ],
            "images" => [
                0 => [
                    "url" => "http;//image.url",
                    "product_class" => Product::class,
                ],
            ]
        ];

        /*
         * 8 mobile data
         *
         */

        $eightMobileLineFirst = [
            'brand' => 'Apple',
            'product_name' => 'Apple Iphone 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'price' => '20.00',
            'non_working_price' => '0.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
            'merchant_category' => 'Mobile phone'
        ];

        $eightMobileLineSecond = [
            'brand' => 'Apple',
            'product_name' => 'Iphone 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'price' => '20.00',
            'non_working_price' => '0.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
            'merchant_category' => 'Apple'
        ];

        $eightMobileResult = [
            "brand" => [
                "name" => "Apple"
            ],
            "product" => [
                "name" => "Apple Iphone 6 32GB",
                "model" => "Iphone 6 32GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "http://example.com",
            ],
            "price" => [
                0 => [
                    "price" => "20.00",
                    "currency" => "GBP",
                    "created_at" => "2017-12-12",
                    "condition" => 1,
                    "network" => 1,
                ],
                1 => [
                    'price' => '0.00',
                    'currency' => 'GBP',
                    'created_at' => "2017-12-12",
                    'condition' => 2,
                    'network' => 1,
                ],
            ],
            "images" => [
                0 => [
                    "url" => "http;//image.url",
                    "product_class" => Product::class,
                ]
            ]
        ];

        /*
         * SimplyDropParser data
         *
         */
        $simplyDropLineFirst = [
            'brand' => 'Apple',
            'product_name' => 'Apple Iphone 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'normal_price' => '20.00',
            'non_working_price' => '0.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
        ];

        $simplyDropLineSecond = [
            'brand' => 'Apple',
            'product_name' => 'Apple 6 32GB',
            'model_number' => 'Iphone 6 32GB',
            'deeplink' => 'http://www.fake?=http://example.com',
            'currency' => 'GBP',
            'normal_price' => '20.00',
            'non_working_price' => '0.00',
            'last_updated' => '2017-12-12',
            'image_url' => 'http;//image.url',
        ];

        $simplyDropLineResult = [
            "brand" => [
                "name" => "Apple"
            ],
            "product" => [
                "name" => "Apple Iphone 6 32GB",
                "model" => "Iphone 6 32GB",
            ],
            "product_profile" => [
                "site_id" => 15
            ],
            "link" => [
                "link" => "http://example.com",
            ],
            "price" => [
                0 => [
                    "price" => "20.00",
                    "currency" => "GBP",
                    "created_at" => "2017-12-12",
                    "condition" => 1,
                    "network" => 1,
                ],
                1 => [
                    'price' => '0.00',
                    'currency' => 'GBP',
                    'created_at' => "2017-12-12",
                    'condition' => 2,
                    'network' => 1,
                ],
            ],
            "images" => [
                0 => [
                    "url" => "http;//image.url",
                    "product_class" => Product::class,
                ]
            ]
        ];

        /*
         * Awin parsers data
         * O2RecycleParser
         */

        $o2RescycleLine = [
            'brand_name' => 'Apple',
            'description' => 'Apple Iphone 6 32GB',
            'merchant_deep_link' => 'http://example.com',
            'currency' => 'GBP',
            'search_price' => '20.00',
            'aw_image_url' => 'http://image.url',
            'merchant_image_url' => 'http://image.thumb.url',
        ];
        $o2RescycleResult = [
            'brand' => [
                'name' => 'Apple',
            ],
            'product' => [
                'name' => 'Apple Iphone 6 32GB',
            ],
            'product_profile' => [
                'site_id' => 15,
            ],
            'link' => [
                'link' => 'http://example.com',
            ],
            'price' => [
                0 => [
                    'price' => '20.00',
                    'currency' => 'GBP',
                    'created_at' => Carbon::today(),
                    'condition' => 1,
                    'network' => 1,
                ],
            ],
            'images' => [
                0 => [
                    'url' => 'http://image.url',
                    'product_class' => Product::class,
                ],
                1 => [
                    'url' => 'http://image.thumb.url',
                    'product_class' => Product::class,
                    'thumb' => true,
                ],
            ],
        ];

        return [
            [RecycleParser::class, $webgainsLine, $webgainsResult],
            [EnvirofoneParser::class, $envirofoneLine, $envirofoneResult],
            [MobileCashParser::class, $mobileCashLines, $mobileCashResult],
            [EightMobileParser::class, $eightMobileLineFirst, $eightMobileResult],
            [EightMobileParser::class, $eightMobileLineSecond, $eightMobileResult],
            [SimplyDropParser::class, $simplyDropLineFirst, $simplyDropLineResult],
            [SimplyDropParser::class, $simplyDropLineSecond, $simplyDropLineResult],
            [O2RecycleParser::class, $o2RescycleLine, $o2RescycleResult],
        ];
    }
}
