<?php

namespace Tests\Unit;

use App\Mail\FailedParsing;
use App\Models\NegativeReport;
use App\Models\PositiveReport;
use App\Models\Site;
use App\Parsers\Exceptions\ParserException;
use App\Parsers\Services\ParseStatisticService;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParserStatisticsServiceTest extends TestCase
{
    /**
     * @var ParseStatisticService
     */
    protected $statisticService;

    /**
     * @var Site
     */
    protected $site;

    public function setUp()
    {
        parent::setUp();
        $this->statisticService = new ParseStatisticService();
        $this->site = factory(Site::class)->create();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->statisticService = null;
        $this->site->delete();
    }

    /**
     *
     */
    public function test_wrong_site_name_exception()
    {
        $this->expectException(ParserException::class);
        $this->statisticService->makeReport('fakeName', $this->getProperData());
    }

    /**
     * @param $data
     * @dataProvider wrongConfigsDataProvider
     */
    public function test_wrong_configuration_exception($data)
    {
        $this->expectException(ParserException::class);
        $this->statisticService->makeReport($this->site->name, $data);
    }

    /**
     * Data provider for test_wrong_configuration_exception
     *
     * @return array
     */
    public function wrongConfigsDataProvider()
    {
        return [
          [$this->getWrongData()]
        ];
    }

    /**
     * @param $data
     * @param $result
     * @dataProvider positiveReportDataProvider
     */
    public function test_making_positive_report($data, $result)
    {
        $this->statisticService->setNotFailed();
        $result['site_id'] = $this->site->id;
        $report = $this->statisticService->makeReport($this->site->parsing_name, $data);

        $this->assertDatabaseHas('positive_report', $result);
        $report->delete();
        $this->assertDatabaseMissing('positive_report', $result);
    }

    /**
     * Data provider for test_making_positive_report
     *
     * @return array
     */
    public function positiveReportDataProvider()
    {
        $data = $this->getProperData();
        $result = $this->getResultData($data);

        return [
          [$data, $result]
        ];
    }

    /**
     * @param $data
     * @dataProvider negativeReportDataProvider
     */
    public function test_making_negative_report($data)
    {
        Mail::fake();
        $this->statisticService->setFailed();
        $report = $this->statisticService->makeReport($this->site->parsing_name, $data);
        Mail::assertSent(FailedParsing::class, function ($mail) use ($report) {
            return $mail->report->id === $report->id;
        });
        $this->assertInstanceOf(NegativeReport::class, $report);
        $report->delete();
    }

    /**
     * Data provider for test_making_negative_report
     *
     * @return array
     */
    public function negativeReportDataProvider()
    {
        return [
            [['reason' => 'hello']],
        ];
    }


    protected function getWrongData()
    {
        return [
            'brand' => 0,
            'price' => 0,
            'nonChanged' => 0,
            'failedLines' => 0,
        ];
    }

    protected function getResultData(array $data)
    {
        $data['product'] += $data['brand'];
        $data['productProfile'] += $data['product'];
        $data['price'] += $data['productProfile'];
        $data['site_id'] = $this->site->id;

        return $data;
    }

    protected function getProperData()
    {
        return [
            'brand' => 2,
            'product' => 6,
            'productProfile' => 10,
            'price' => 12,
            'nonChanged' => 100,
            'failedLines' => 0,
        ];
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
