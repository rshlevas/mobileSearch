<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Parsers\Exceptions\EntityServiceException;
use App\Parsers\Services\EntityChecker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EntityCheckerTest extends TestCase
{
    private $checker;

    private $brand;

    private $params = [
        'brand' => Brand::class
    ];

    public function setUp()
    {
        parent::setUp();
        $this->checker = new EntityChecker();
        $this->brand = $this->getBrand();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->checker = null;
        $this->brand->delete();
        $this->brand = null;
        $this->params = null;
    }


    /**
     * Test the exceptions
     *
     * @dataProvider exceptionDataProvider
     * @return void
     */
    public function test_expect_exceptions($params = null, $arg1 = null, $arg2 = null)
    {
        $this->expectException(EntityServiceException::class);
        $this->checker->brand();
    }


    public function exceptionDataProvider()
    {
        return [
            [],
            [$this->params],
            [$this->params, 'foo', 'bar']

        ];
    }

    /**
     * @dataProvider checkParamsDataProvider
     */
    public function test_check_function($where, $result = null)
    {
        $this->checker->setParams($this->params);

        $brand = $this->checker->brand($where);
        $this->assertEquals($result, $brand);
    }

    public function checkParamsDataProvider()
    {
        $where = ['name' => $this->brand->name];

        return [
            [$where, $this->brand],
            [['name' => 'fake']]
        ];
    }

    protected function getBrand()
    {
        $brand = factory(Brand::class)->create([
            'name' => 'Nokia',
            'url_name' => 'nokia'
        ]);

        return $brand;
    }


}
