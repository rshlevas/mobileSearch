<?php

namespace Tests\Unit;

use App\Models\SellLink;
use App\Services\LinkTransformer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class LinkTransformerTest extends TestCase
{
    /**
     * @var LinkTransformer
     */
    protected $service;

    protected $link;

    public function setUp()
    {
        parent::setUp();
        $this->service = new LinkTransformer();
        $this->link = $this->getLink();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->service = null;
        $this->link = null;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_run_transform()
    {
        $result = $this->service->run($this->link->link);
        $this->assertEquals($this->getProperLink(), $result);
    }

    protected function getLink()
    {
        return factory(SellLink::class)->make();
    }

    protected function getProperLink()
    {
        return config('skimlinks.domain').'/?id='.config('skimlinks.key').'&xs='.config('skimlinks.xs').'&url='.urlencode($this->link->link);
    }
}
