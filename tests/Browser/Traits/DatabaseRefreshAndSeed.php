<?php

namespace Tests\Browser\Traits;

use Illuminate\Contracts\Console\Kernel;

trait DatabaseRefreshAndSeed
{
    /**
     * Refresh and seed DB after each test.
     *
     * @return void
     */
    public function runDatabaseRefreshing()
    {
        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:refresh');
            $this->artisan('db:seed');
        });
    }
}