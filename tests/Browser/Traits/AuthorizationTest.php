<?php

namespace Tests\Browser\Traits;


use Laravel\Dusk\Browser;

trait AuthorizationTest
{
    /**
     * Not authorized user don't allow to be there.
     *
     * @dataProvider AuthorizationLinksDataProvider
     * @return void
     */
    public function testNotAuthorized($link)
    {
        $this->browse(function (Browser $browser) use ($link) {
            $browser->visit('/logout')->logout();
            $browser->visit($link)
                ->assertPathIs('/login');
        });
    }

    public function AuthorizationLinksDataProvider()
    {
        return [
            ['/admin']
        ];
    }
}