<?php

namespace Tests\Browser\Traits;

use Illuminate\Database\Eloquent\Model;

trait ModelManagable
{
    /**
     * Gets entity from DB
     *
     * @param string $className
     * @return mixed
     */
    protected function getModelByClassName(string $className)
    {
        return $className::take(1)->get()->first();
    }

    /**
     * Creates entity
     *
     * @param string $className
     * @param array $params
     * @return Model
     */
    protected function createEntityByClassName(string $className, array $params)
    {
        return factory($className)->create($params);
    }

    /**
     * Delete entity from DB
     *
     * @param Model $entity
     * @return mixed
     */
    protected function deleteEntity(Model $entity)
    {
        return $entity->delete();
    }
}