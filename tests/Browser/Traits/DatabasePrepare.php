<?php

namespace Tests\Browser\Traits;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductProfile;
use App\Models\SellLink;
use App\Models\Site;
use Illuminate\Support\Collection;

trait DatabasePrepare
{
    use ModelManagable;

    /**
     * Seeding DB by necessary data for test
     */
    public function seedDB()
    {
        $product = $this->createProduct($this->createBrand());
        $this->createProductProfiles($product, $this->getSites());
        $this->createProfilesRelations($this->getProfiles());
    }

    /**
     * Creates product profiles for product
     *
     * @param Product $product
     * @param Collection $sites
     */
    protected function createProductProfiles(Product $product, Collection $sites)
    {
        $data = ['product_id' => $product->id];
        foreach ($sites as $site) {
            $data['site_id'] = $site->id;
            $this->createEntityByClassName(ProductProfile::class, $data);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getProfiles()
    {
        return ProductProfile::all();
    }

    /**
     * @param Collection $productProfiles
     */
    protected function createProfilesRelations(Collection $productProfiles)
    {
        foreach ($productProfiles as $profile) {
            $this->createEntityByClassName(SellLink::class, ['product_profile_id' => $profile->id]);
            $this->createEntityByClassName(ProductPrice::class, ['product_profile_id' => $profile->id]);
        }
    }

    /**
     * @return mixed
     */
    protected function getSites()
    {
        return Site::take(5)->get();
    }

    /**
     * @param $brand
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function createProduct($brand)
    {
        $data = [
            'name' => 'Nokia 1100',
            'url_name' => 'nokia_1100',
            'check_name' => '1100 nokia',
            'brand_id' => $brand->id,
            'model' => '1100'
        ];

        return $this->createEntityByClassName(Product::class, $data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function createBrand()
    {
        return $this->createEntityByClassName(Brand::class, [
            'name' => 'Nokia',
            'url_name' => 'nokia'
        ]);
    }


}