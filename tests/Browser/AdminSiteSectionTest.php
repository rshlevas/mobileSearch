<?php

namespace Tests\Browser;

use App\Models\Site;
use App\User;
use Tests\Browser\Traits\AuthorizationTest;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminSiteSectionTest extends DuskTestCase
{
    use AuthorizationTest;

    public function AuthorizationLinksDataProvider()
    {
        return [
          ['/admin/sites'],
          ['/admin/sites/create'],
        ];
    }

    /**
     * Test site section page
     *
     * @return void
     */
    public function testAdminSiteSection()
    {
        $user = User::find(1);
        $site = Site::find(1);

        $this->browse(function (Browser $browser) use ($user, $site) {
            $browser->loginAs($user)
                ->visit('/admin/sites')
                ->assertSee('Site section managing')
                ->assertSee($site->name);
        });
    }

    /**
     * Test site updating
     *
     * @return void
     */
    public function testAdminSiteUpdateSection()
    {
        $user = User::find(1);
        $site = Site::find(1);
        $otherSite = Site::find(2);
        $currentName = $site->name;
        $newName = 'Novel';
        $otherSiteName = $otherSite->name;

        $this->browse(function (Browser $browser) use ($user, $currentName, $newName, $otherSiteName, $site) {
            $browser->loginAs($user)
                ->visit('/admin/sites')
                ->assertSee('Site section managing')
                ->assertVisible('.icon-pencil')
                ->click('.icon-pencil')
                ->assertPathIs('/admin/sites/update/' . $site->parsing_name)
                ->assertSee("Update Site: ". ucfirst($currentName))
                ->type('name', $otherSiteName)
                ->press('Update')
                ->assertSee('The name has already been taken.')
                ->type('name', $newName)
                ->press('Update')
                ->assertPathIs('/admin/sites')
                ->assertSee($newName)
                ->click('.icon-pencil')
                ->type('name', $currentName)
                ->press('Update')
                ->assertSee($currentName);
        });
    }

    /**
     * Test site creating validation
     *
     * @return void
     */
    public function testAdminSiteCreateValidationSection()
    {
        $site = Site::find(1);
        $user = User::find(1);
        $siteName = $site->name;
        $link = 'example';

        $this->browse(function (Browser $browser) use ($user, $siteName, $link) {
            $browser->loginAs($user)
                ->visit('/admin/sites')
                ->assertSee('Site section managing')
                ->clickLink('+ Add new site')
                ->assertPathIs('/admin/sites/create')
                ->assertSee('Add Site')
                ->type('name', $siteName)
                ->type('path', $link)
                ->press('Create')
                ->assertSee('The name has already been taken.')
                ->assertSee('The path format is invalid.');
        });
    }

    /**
     * Test site creating
     *
     * @return void
     */
    public function testAdminSiteCreatingSection()
    {
        $user = User::find(1);
        $siteName = 'Novel';
        $link = 'example.com';

        $this->browse(function (Browser $browser) use ($user, $siteName, $link) {
            $browser->loginAs($user)
                ->visit('/admin/sites')
                ->assertSee('Site section managing')
                ->clickLink('+ Add new site')
                ->assertPathIs('/admin/sites/create')
                ->assertSee('Add Site')
                ->type('name', $siteName)
                ->type('path', $link)
                ->press('Create')
                ->assertPathIs('/admin/sites')
                ->assertSee($siteName);
        });
    }

    /**
     * Test site deleting
     *
     * @return void
     */
    public function testAdminSiteDeletingSection()
    {
        $user = User::find(1);
        $site = Site::where('name', 'Novel')->get()->first();

        $this->browse(function (Browser $browser) use ($user, $site) {
            $browser->loginAs($user)
                ->visit('/admin/sites')
                ->assertSee('Site section managing')
                ->assertSee($site->name)
                ->visit("/admin/sites/delete/{$site->parsing_name}")
                ->assertPathIs('/admin/sites')
                ->assertDontSee($site->name);
        });
    }
}
