<?php

namespace Tests\Browser;

use App\Models\Payment;
use App\Models\PaymentTerm;
use App\User;
use Tests\Browser\Traits\AuthorizationTest;
use Tests\Browser\Traits\ModelManagable;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminPaymentsSectionTest extends DuskTestCase
{
    use AuthorizationTest, ModelManagable;

    public function AuthorizationLinksDataProvider()
    {
        return [
            ['/admin/payments'],
            ['/admin/payments/method/create'],
            ['/admin/payments/method/update/1'],
        ];
    }

    /**
     * Payment section main page test.
     *
     * @return void
     */
    public function testPaymentSectionMainPage()
    {
        $user = $this->getModelByClassName(User::class);
        $method = $this->getModelByClassName(Payment::class);
        $term = $this->getModelByClassName(PaymentTerm::class);

        $this->browse(function (Browser $browser) use ($user, $method, $term){
            $browser->loginAs($user)
                ->visit('/admin/payments/')
                ->assertSee('Payments section managing')
                ->assertSee($method->name)
                ->assertSee($term->name);
        });
    }

    /**
     * Payment method and term creation test
     *
     * @dataProvider paymentCreationDataProvider
     * @return void
     */
    public function testPaymentCreation($className, $alias, $name)
    {
        $this->deleteModelIfExist($name, $className);
        $user = $this->getModelByClassName(User::class);
        $model = $this->getModelByClassName($className);

        $this->browse(function (Browser $browser) use ($user, $model, $name, $alias) {
            $browser->loginAs($user)
                ->visit('/admin/payments/')
                ->assertSee('Payments section managing')
                ->visit("/admin/payments/{$alias}/create/")
                ->assertSee('Create Payment')
                ->type('name', $model->name)
                ->press('Create')
                ->assertSee('The name has already been taken.')
                ->type('name', $name)
                ->press('Create')
                ->assertPathIs('/admin/payments')
                ->assertSee($name);
        });
    }

    public function paymentCreationDataProvider()
    {
        return [
            [Payment::class, 'method', 'Novel'],
            [PaymentTerm::class, 'term', 'Term'],
        ];
    }

    /**
     * Payment method and term updating test
     *
     * @dataProvider paymentUpdatingDataProvider
     * @return void
     */
    public function testPaymentUpdating($className, $alias, $name)
    {
        $this->deleteModelIfExist($name, $className);
        $user = $this->getModelByClassName(User::class);
        $model = $this->getModelByClassName($className);

        $this->browse(function (Browser $browser) use ($user, $model, $name, $alias) {
            $browser->loginAs($user)
                ->visit('/admin/payments/')
                ->assertSee('Payments section managing')
                ->visit("/admin/payments/{$alias}/update/{$model->id}")
                ->assertSee('Update Payment')
                ->type('name', $name)
                ->press('Update')
                ->assertPathIs('/admin/payments')
                ->assertSee($name)
                ->visit("/admin/payments/{$alias}/update/{$model->id}")
                ->type('name', $model->name)
                ->press('Update')
                ->assertPathIs('/admin/payments');
        });
    }

    public function paymentUpdatingDataProvider()
    {
        return [
            [Payment::class, 'method', 'Novel'],
            [PaymentTerm::class, 'term', 'Term'],
        ];
    }

    protected function deleteModelIfExist(string $name, string $className)
    {
        $payment = $className::where(['name' => $name])->get()->first();
        if ($payment) {
            return $payment->delete();
        }

        return $payment;
    }
}
