<?php

namespace Tests\Browser;

use App\Models\Product;
use App\Models\ProductPrice;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserProductTest extends DuskTestCase
{
    /**
     * Product view page test
     *
     * @return void
     */
    public function testProductViewPage()
    {
        $product = Product::take(1)->get()->first();
        $profiles = $product->profiles;
        $prices = collect([]);
        foreach ($profiles as $profile) {
            $prices->push($profile->bestPrice);
        }

        $this->browse(function (Browser $browser) use ($product, $prices) {
            $browser->visit("/sell/{$product->url_name}")
                    ->assertSee($product->name)
                    ->assertSee($product->brand->name);

            foreach ($prices as $price) {
                $this->shouldSeePrice($price, $browser);
            }
        });
    }

    protected function shouldSeePrice(ProductPrice $price, Browser $browser)
    {
        $browser->assertSee($price->price);
    }
}
