<?php

namespace Tests\Browser;

use App\Models\Brand;
use App\Models\Product;
use App\User;
use Tests\Browser\Traits\DatabasePrepare;
use Tests\Browser\Traits\DatabaseRefreshAndSeed;
use Tests\Browser\Traits\ModelManagable;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminProductSearchTest extends DuskTestCase
{
    use DatabasePrepare, DatabaseRefreshAndSeed;

    public function setUp()
    {
        parent::setUp();
        //$this->seedDB();
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testProductSearch()
    {
        $product = Product::find(1);
        $user = User::find(1);
        $searchName = substr($product->name, 0, strlen($product->name) - 1);

        $this->browse(function (Browser $browser) use ($product, $searchName, $user) {
            $browser->loginAs($user)
                ->visit('/admin/search')
                ->assertSee('MobileSearch')
                ->type('search', $searchName)
                ->press('Search')
                ->assertPathIs('/admin/search/result')
                ->assertSee('Search Result')
                ->assertSee($product->brand->name)
                ->assertSee($product->model);
        });

        //$this->runDatabaseRefreshing();
    }
}
