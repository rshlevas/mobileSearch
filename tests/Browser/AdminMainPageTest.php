<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Traits\AuthorizationTest;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminMainPageTest extends DuskTestCase
{
    use AuthorizationTest;
    /**
     * Test of admin main page and all available sections
     *
     * @return void
     */
    public function testMainPageSections()
    {
        $user = User::find(1);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin')
                ->assertPathIs('/admin')
                ->assertSee($user->name)
                ->assertSee('Parsing reports');

            foreach ($this->pages as $page) {
                $this->visitLink($browser, $page['link'], $page['path'], $page['redirect']);
            }
        });
    }

    /**
     * @var array
     */
    protected $pages = [
        0 => [
            'link' => 'Sites section',
            'path' => '/admin/sites',
            'redirect' => '/admin',
        ],
        1 => [
            'link' => 'Brands section',
            'path' => '/admin/brands',
            'redirect' => '/admin',
        ],
        2 => [
            'link' => 'Payments section',
            'path' => '/admin/payments',
            'redirect' => '/admin',
        ],
        3 => [
            'link' => 'Product Search',
            'path' => '/admin/search',
            'redirect' => '/admin',
        ],
        4 => [
            'link' => 'Most popular section',
            'path' => '/admin/most-popular',
            'redirect' => '/admin',
        ],
        5 => [
            'link' => 'Products without images',
            'path' => '/admin/no-image',
            'redirect' => '/admin',
        ],

    ];

    /**
     * Visit link function
     *
     * @param Browser $browser
     * @param $link
     * @param $path
     * @param $redirect
     * @return Browser
     */
    protected function visitLink(Browser $browser, $link, $path, $redirect)
    {
        $browser->clickLink($link)
            ->assertPathIs($path)
            ->visit($redirect);

        return $browser;
    }
}