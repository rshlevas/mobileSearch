<?php

namespace Tests\Browser;

use App\Models\Brand;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserBrandTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testBrandSectionPages()
    {
        $brand = Brand::with('products')->take(1)->get()->first();
        $product = $brand->products->first();

        $this->browse(function (Browser $browser) use ($brand, $product) {
            $browser->visit('/brands')
                ->assertSee($brand->name)
                ->clickLink($brand->name)
                ->assertPathIs("/brands/{$brand->url_name}")
                ->assertSee($brand->name)
                ->assertSee("We have " . count($brand->products))
                ->assertSee($product->model);
        });
    }
}
