<?php

namespace Tests\Browser;

use App\Models\Product;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserSearchTest extends DuskTestCase
{
    /**
     * Search function test.
     *
     * @return void
     */
    public function testSearchByShorterProductName()
    {
        $product = Product::take(1)->get()->first();

        $searchName = substr($product->name, 0, strlen($product->name) - 1);

        $this->browse(function (Browser $browser) use ($product, $searchName) {
            $browser->visit('/')
                    ->assertSee('MobileSearch')
                    ->type('search', $searchName)
                    ->waitForText($product->name)
                    ->press('.btn-lg')
                    ->assertPathIs('/search')
                    ->assertSee('Search Result')
                    ->assertSee($product->brand->name)
                    ->assertSee($product->model)
                    ->assertSee($product->best_price->price);
        });
    }
}
