<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class GroupWithSitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 1;
        $info = Config::get('parser');
        foreach ($info['sites'] as $group => $sites) {
            factory(\App\Models\Group::class)->create(['name' => $group]);
            foreach ($sites as $name => $data) {
                factory(\App\Models\Site::class)->create([
                   'name' => $name,
                    'api_link' => $data['api_link'],
                    'path' => $data['path'],
                    'parsing_name' => $data['parsing_name'],
                    'group_id' => $count,
                ]);
            }
            $count++;
        }
    }
}
