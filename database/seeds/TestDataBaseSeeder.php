<?php

use Illuminate\Database\Seeder;

class TestDataBaseSeeder extends Seeder
{
    use \Tests\Browser\Traits\ModelManagable;

    protected $brandInfo = [
        0 => [
            'name' => 'Nokia',
            'url_name' => 'nokia'
        ],
        1 => [
            'name' => 'Apple',
            'url_name' => 'apple'
        ],
        2 => [
            'name' => 'Samsung',
            'url_name' => 'samsung'
        ],
    ];

    protected $productInfo = [
        0 => [
            'name' => 'Nokia 1100',
            'url_name' => 'nokia_1100',
            'check_name' => '1100 nokia',
            'brand_id' => 1,
            'model' => '1100',
        ],
        1 => [
            'name' => 'Apple Iphone 5',
            'url_name' => 'apple_iphone_5',
            'check_name' => '5 apple iphone',
            'brand_id' => 2,
            'model' => 'Iphone 5',
        ],
        2 => [
            'name' => 'Samsung Galaxy Tab',
            'url_name' => 'samsung_galaxy_tab',
            'check_name' => 'galaxy samsung tab',
            'brand_id' => 3,
            'model' => 'Galaxy Tab',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->brandInfo as $brand) {
            $this->createEntityByClassName(\App\Models\Brand::class, $brand);
        }

        foreach ($this->productInfo as $product) {
            $product = $this->createEntityByClassName(\App\Models\Product::class, $product);
            $this->createProductProfiles($product, $this->getSites());
        }

        $this->createProfilesRelations($this->getProfiles());
    }

    /**
     * @param \App\Models\Product $product
     * @param \Illuminate\Support\Collection $sites
     */
    protected function createProductProfiles(\App\Models\Product $product, \Illuminate\Support\Collection $sites)
    {
        $data = ['product_id' => $product->id];
        foreach ($sites as $site) {
            $data['site_id'] = $site->id;
            $this->createEntityByClassName(\App\Models\ProductProfile::class, $data);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getProfiles()
    {
        return \App\Models\ProductProfile::all();
    }

    /**
     * @param \Illuminate\Support\Collection $productProfiles
     */
    protected function createProfilesRelations(\Illuminate\Support\Collection $productProfiles)
    {
        foreach ($productProfiles as $profile) {
            $this->createEntityByClassName(\App\Models\SellLink::class, ['product_profile_id' => $profile->id]);
            $this->createEntityByClassName(\App\Models\ProductPrice::class, ['product_profile_id' => $profile->id]);
        }
    }

    /**
     * @return mixed
     */
    protected function getSites()
    {
        return \App\Models\Site::take(5)->get();
    }
}
