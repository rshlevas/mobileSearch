<?php

use Illuminate\Database\Seeder;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quiz = config('quiz.questions');
        $count = 1;
        foreach ($quiz as $item) {
            factory(\App\Models\Question::class)->create($item['question']);
            foreach ($item['answers'] as $answer) {
                $answer['question_id'] = $count;
                factory(\App\Models\Answer::class)->create($answer);
            }
            $count++;
        }
    }
}
