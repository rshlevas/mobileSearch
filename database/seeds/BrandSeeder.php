<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Brand::class, 5)->create()->each(function($b) {
            for ($i = 1; $i < 4; $i++) {
                $b->products()->save(factory(\App\Models\Product::class)->create([
                    'brand_id' => $b->id,
                ]));
            }
        });
    }
}
