<?php

use Illuminate\Database\Seeder;

class PaymentTermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Same Day', 'Next Day', '2 Days'];

        foreach ($names as $name) {
            factory(\App\Models\PaymentTerm::class)->create(['name' => $name]);
        }
    }
}
