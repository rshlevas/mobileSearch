<?php

use Illuminate\Database\Seeder;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 3; $i++) {
            for ($j = 1; $j < 46; $j++) {
                factory(\App\Models\ProductPrice::class)->create([
                    'product_profile_id' => $j
                ]);
            }
        }
    }
}
