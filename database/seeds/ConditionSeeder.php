<?php

use Illuminate\Database\Seeder;

class ConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('filter.seeding.condition') as $lang => $conditions) {
            foreach($conditions as $key => $condition) {
                factory(\App\Models\Condition::class)->create([
                    'name' => $condition['display'],
                    'alias' => $condition['alias'],
                    'key' => $key,
                    'min_weight' => $condition['min_weight'],
                    'max_weight' => $condition['max_weight'],
                    'lang' => $lang,
                ]);
            }
        }
    }
}
