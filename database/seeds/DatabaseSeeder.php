<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create(['email' => 'admin@example.com']);
        $this->call([
            PaymentSeeder::class,
            PaymentTermSeeder::class,
            GroupWithSitesSeeder::class,
            ConditionSeeder::class,
            QuizSeeder::class,

            /*BrandSeeder::class,
            SiteSeeder::class,
            ProductProfileSeeder::class,
            PricesSeeder::class,
            ParseReportSeeder::class,*/
        ]);
    }
}
