<?php

use App\Models\Brand;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'url_name' => $faker->unique()->word,
    ];
});
