<?php

use Faker\Generator as Faker;
use App\Models\Image;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'path' => asset('images').'/no-image.jpeg'
    ];
});
