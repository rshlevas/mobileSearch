<?php

use Faker\Generator as Faker;
use App\Models\PositiveReport;

$factory->define(PositiveReport::class, function (Faker $faker) {
    return [
        'site_id' => 1,
        'brand' => $faker->numberBetween(1, 5),
        'product' => $faker->numberBetween(6, 50),
        'productProfile' => $faker->numberBetween(6, 50),
        'price' => $faker->numberBetween(6, 50),
        'nonChanged' => $faker->numberBetween(100, 200),
        'failedLines' => 0,
        'created_at' => $faker->dateTimeBetween('-1 year'),
    ];
});
