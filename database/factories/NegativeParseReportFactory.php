<?php

use Faker\Generator as Faker;
use App\Models\PositiveReport;


$factory->define(\App\Models\NegativeReport::class, function (Faker $faker) {
    return [
        'site_id' => 1,
        'reason' => $faker->text(100),
        'created_at' => $faker->dateTimeBetween('-1 year'),
    ];
});
