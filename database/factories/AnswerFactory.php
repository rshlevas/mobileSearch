<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Answer::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence,
        'weight' => 1,
        'lang' => 'en',
        'question_id' => 1,
    ];
});
