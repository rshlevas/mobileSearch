<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Question::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence,
        'lang' => 'en',
        'order' => 0,
    ];
});
