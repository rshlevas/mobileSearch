<?php

use App\Models\ProductProfile;
use Faker\Generator as Faker;

$factory->define(ProductProfile::class, function (Faker $faker) {
    return [
        'product_id' => 1,
        'site_id' => 1,
    ];
});
