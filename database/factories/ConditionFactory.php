<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Condition::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'alias' => $faker->word,
        'lang' => 'en',
        'key' => 1,
    ];
});
