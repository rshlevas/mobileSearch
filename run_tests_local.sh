#!/usr/bin/env sh

#Running unit tests
echo "Running unit tests"
php artisan migrate:refresh --seed --env=testing
vendor/bin/phpunit tests/Unit

#Running browser tests
echo "Running browser tests"
php artisan migrate:refresh --seed --env=testing
php artisan db:seed --class=TestDataBaseSeeder --env=testing
php artisan dusk