<?php

namespace App\Traits;

use Illuminate\Support\Collection;

trait ProductSortable
{
    /**
     * @param Collection $products
     * @return Collection
     */
    protected function sortByPriceDesc(Collection $products)
    {
        return $products->sortByDesc(function($product) {
            return $product->bestPrice->price;
        });
    }
}