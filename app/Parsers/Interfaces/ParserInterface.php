<?php

namespace App\Parsers\Interfaces;


interface ParserInterface
{
    public function run();
}