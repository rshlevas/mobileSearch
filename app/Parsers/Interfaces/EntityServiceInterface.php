<?php

namespace App\Parsers\Interfaces;

interface EntityServiceInterface
{
    /**
     * Current function have to describe the
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments);

    /**
     * Function that configure the EntityService
     *
     * @param array $params
     * @return mixed
     */
    public function setParams(array $params);
}