<?php

namespace App\Parsers\JSONParser\CustomParsers;

class GeckoMobileParser extends CustomParser
{
    /**
     * @var bool
     */
    protected $imagable = false;

    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $priceFilters;

    /**
     * @var
     */
    protected $priceDate;


    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        $this->setPriceDate($line['updated_at']);

        return [
            'brand' => [
                'name' => $this->prepareBrandName($line['brand']),
            ],
            'product' => [
                'name' => $this->prepareProductName($line['product_name']),
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['url'],
            ],
            'price' => $this->preparePrices($line['prices']),
        ];
    }

    /**
     * Adding Apple brand to Iphone products
     *
     * @param $name
     * @return string
     */
    protected function prepareProductName($name)
    {
        return strpos($name, 'iPhone') === 0 ? "Apple {$name}" : $name;
    }
}