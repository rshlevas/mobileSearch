<?php

namespace App\Parsers\JSONParser\CustomParsers;

class FonebankParser extends CustomParser
{
    protected $customFilters = [
        0 => [
            'method' => 'str_replace',
            'needle' => 'Caterpillar',
            'replace' => 'CAT',
        ],
    ];

    protected function prepareData(array $line)
    {
        $this->setPriceDate($line['updated_at']);

        return [
            'brand' => [
                'name' => $this->prepareBrandName($line['brand']),
            ],
            'product' => [
                'name' => $this->prepareProductName($line['product_name']),
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['url'],
            ],
            'price' => $this->preparePrices($line['prices']),
        ];
    }

    protected function prepareProductName($name)
    {
        return strpos($name, 'i Mate') === 0 ? str_replace('i Mate', 'I-mate', $name) : $name;
    }
}