<?php

namespace App\Parsers\Services;

use App\Models\NegativeReport;
use App\Models\PositiveReport;
use App\Models\Site;
use App\Parsers\Exceptions\ParserException;
use Carbon\Carbon;

class ParseStatisticService
{
    /**
     * Flag for switching the class mode
     *
     * @var bool
     */
    protected $failed = false;

    /**
     *
     * @var
     */
    protected $data;

    /**
     * Example of necessary data, that should be given for positive report
     *
     * @var array
     */
    protected $positiveDataMap = ['brand', 'failedLines', 'nonChanged', 'price', 'product', 'productProfile'];

    /**
     * Example of necessary data, that should be given for negative report
     *
     * @var array
     */
    protected $negativeDataMap = ['reason'];

    /**
     * Function that starts generating of parsing report for given site
     * Throws exception if site doesn't present at DB and wrong site name given
     * $data param should contain different keys depending on ParseStatisticService mode.
     * If you make negative report You should use setFailed method in this case before making report
     *
     * @param string $siteName
     * @param array $data
     * @return mixed
     * @throws ParserException
     */

    public function makeReport(string $siteName, array $data)
    {
        $site = $this->getSite($siteName);
        if ( ! $site) {
            throw new ParserException("There is no {$siteName} in DB");
        }
        $this->setData($data);

        return $this->report($site);
    }

    /**
     * Function, that checks data, that should be present at the report.
     * Throws exception if data is not given and current class doesn't configured correctly
     *
     * @param null $data
     * @return null
     * @throws ParserException
     */
    protected function setData($data)
    {
        if ($data) {
            $data = $this->checkDataStructure($data);
        }

        if ( ! $data) {
            throw new ParserException("No proper data given. If parser failed configure ". get_class($this) . " correctly");
        }

        return $this->data = $data;
    }

    /**
     * Function, that checks given data structure
     *
     * @param $data
     * @return null
     */
    protected function checkDataStructure($data)
    {
        $dataKeys = array_keys($data);
        sort($dataKeys);

        if ($dataKeys == $this->positiveDataMap && ! $this->failed) {
            return $this->checkDataContent($data);
        } elseif ($dataKeys == $this->negativeDataMap && $this->failed) {
            return $data;
        }

        return null;
    }

    /**
     * Check if data content, consists not only from zeros
     *
     * @param $data
     * @return null
     */
    protected function checkDataContent($data)
    {
        foreach ($data as $item) {
            if ($item !== 0) {
                return $data;
            }
        }
        $this->failed = true;

        return [
            'reason' => 'Parsing file have no content'
        ];
    }


    /**
     * Set current class at failed mode.
     * At this state class will generate negative report
     *
     * @return bool
     */
    public function setFailed()
    {
        return $this->failed = true;
    }

    /**
     * Set current class at not failed mode
     * At this state class will generate positive report
     *
     * @return bool
     */
    public function setNotFailed()
    {
        return $this->failed = false;
    }

    /**
     * Look for site object at DB. Returns null if it absent.
     *
     * @param string $name
     * @return Site
     */
    protected function getSite(string $name)
    {
        return Site::where('parsing_name', $name)->get()->first();
    }

    /**
     * Function, that starts report generating. Need for encapsulation of data
     *
     * @param Site $site
     * @return mixed
     */
    protected function report(Site $site)
    {
        $this->prepareData($site);
        $this->setParseDate($site);
        if ($this->failed) {
            return $this->createNegativeReport();
        }

        return $this->createPositiveReport();
    }

    /**
     * Update date of last parsing at site entity
     *
     * @param Site $site
     * @return bool
     */
    protected function setParseDate(Site $site)
    {
        return $site->update(['last_parsing' => Carbon::now()]);
    }

    /**
     * Prepared data before report generation. Adds additional params, that
     * are necessary for proper generation
     *
     * @param Site $site
     */
    protected function prepareData(Site $site)
    {
        $this->data['site_id'] = $site->id;

        if ( ! $this->failed) {
            $this->calculateDataParams();
        }

        return;
    }

    /**
     * Calculate necessary params, for true report info
     *
     * @return void
     */
    protected function calculateDataParams()
    {
        $this->data['product'] += $this->data['brand'];
        $this->data['productProfile'] += $this->data['product'];
        $this->data['price'] += $this->data['productProfile'];

        return;
    }

    /**
     * Creates PositiveReport entity with set params.
     *
     * @return mixed
     */
    protected function createPositiveReport()
    {
        return PositiveReport::create($this->data);
    }

    /**
     * Creates NegativeReport entity with set params.
     *
     * @return mixed
     */
    protected function createNegativeReport()
    {
        return NegativeReport::create($this->data);
    }

}