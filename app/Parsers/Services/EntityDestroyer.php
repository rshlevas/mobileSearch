<?php

namespace App\Parsers\Services;

class EntityDestroyer extends EntityService
{

    /**
     * Set type of db operation
     */
    protected function setType()
    {
        $this->type = 'where';
    }

    /**
     * @param array $where
     * @return mixed
     */
    protected function run(array $where)
    {
        $qb = parent::run($where);

        return $qb->delete();
    }
}