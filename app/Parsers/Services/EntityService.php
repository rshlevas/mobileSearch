<?php

namespace App\Parsers\Services;

use App\Parsers\Exceptions\EntityServiceException;
use App\Parsers\Interfaces\EntityServiceInterface;

abstract class EntityService implements EntityServiceInterface
{
    /**
     * String type, that show how to interact with db:
     * create,
     * update,
     * where etc
     *
     * @var
     */
    protected $type;

    /**
     * Array with the class params
     * should have such structure
     *  [ 'modelName' => 'EloquentModelClassName' ]
     *
     * @var array
     */
    protected $params = [];

    /**
     * @var
     */
    protected $model;

    /**
     * This function check the called method depending on params
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws EntityServiceException
     */
    public function __call($name, $arguments)
    {
        $this->checkParams();
        $this->checkArguments($arguments);
        if (isset($this->params[$name])) {
            $this->setModel($this->params[$name]);
            $this->setType();
            return $this->run($arguments[0]);
        }

        throw new EntityServiceException('You called no identified method or send too much arguments');
    }

    /**
     * Should to called before any other methods called
     *
     * @param array $params
     *
     * @return array
     */
    public function setParams(array $params)
    {
        return $this->params = $params;
    }

    /**
     * Check the arguments
     *
     * @param $arguments
     * @return bool
     * @throws EntityServiceException
     */
    protected function checkArguments($arguments)
    {
        if ($this->checkArgumentsNumber(count($arguments)) && $this->checkArgumentsType($arguments[0])) {
            return true;
        }

        return false;
    }

    /**
     * Check argument type
     *
     * @param $argument
     * @return bool
     * @throws EntityServiceException
     */
    protected function checkArgumentsType($argument)
    {
        if (! is_array($argument) ) {
            throw new EntityServiceException('Given argument should be an array, but' . gettype($argument) . 'was given');
        }

        return true;
    }

    /**
     * CHeck the arguments count
     *
     * @param int $count
     * @return bool
     * @throws EntityServiceException
     */
    protected function checkArgumentsNumber(int $count)
    {
        if ($count > 1 ) {
            throw new EntityServiceException('Too much arguments were given');
        } elseif ($count < 1) {
            throw new EntityServiceException('Too few arguments were given');
        }

        return true;
    }


    /**
     * Check if params is set
     *
     * @return bool
     * @throws EntityServiceException
     */
    protected function checkParams()
    {
        if (empty($this->params)) {
            throw new EntityServiceException(get_class($this) . ' is not configured');
        }

        return true;
    }

    /**
     *
     * @param string $modelName
     * @return string
     */
    protected function setModel(string $modelName)
    {
        return $this->model = $modelName;
    }

    /**
     * FUnction that starts the interaction with db, if it used update or where type
     * method will return QueryBuilder object, though during extending it should be transformed
     *
     * @param array $where
     * @return mixed
     */
    protected function run(array $where)
    {
        return $this->model::{$this->type}($where);
    }

    /**
     * Set type of db operation
     */
    abstract protected function setType();
}