<?php

namespace App\Parsers\Services;

class EntityCreator extends EntityService
{
    protected function setType()
    {
        $this->type = 'create';
    }
}