<?php

namespace App\Parsers\Services;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class ModelService
{
    /**
     * Map that used, for configuring of Checker and Creator
     * and for configuring of current class behaviour
     *
     * @var array
     */
    protected $configs;

    /**
     * @var EntityChecker
     */
    protected $checker;

    /**
     * @var EntityCreator
     */
    protected $creator;

    /**
     * @var EntityDestroyer
     */
    protected $destroyer;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @var
     */
    protected $data;

    /**
     * @var
     */
    protected $withImages;

    /**
     * ModelService constructor.
     * @param EntityChecker $checker
     * @param EntityCreator $creator
     * @param EntityDestroyer $destroyer
     * @param ImageService $imageService
     */
    public function __construct(EntityChecker $checker,
                                EntityCreator $creator,
                                EntityDestroyer $destroyer,
                                ImageService $imageService)
    {
        $this->checker = $checker;
        $this->creator = $creator;
        $this->destroyer = $destroyer;
        $this->imageService = $imageService;
        $this->configure();
    }

    /**
     * Starts the interaction of data obtained from parser with DB.
     * It check the presence of part of the data in the DB and if it necessary
     * add them.
     * Order of data entities checking depends on the order of keys in the $model array
     *
     * @param array $data
     * @return string
     */
    public function run(array $data, $withImages = false)
    {
        $this->setData($data, $withImages);

        foreach (array_keys($this->configs) as $key => $step) {
            $check = 'check' . ucfirst($step);
            $model = $this->{$check}();

            if (! $model) {
                $create = 'create' . ucfirst($step);
                $this->{$create}();
                return $step;
            } elseif ($step == 'price') {
                return 'nonChanged';
            }

            $this->updateData($model, $step);
        }

        return 'nonChanged';
    }

    /**
     * Sets obtained data
     *
     * @param array $data
     * @return array
     */
    protected function setData(array $data, bool $withImages)
    {
        $this->withImages = $withImages;
        return $this->data = $data;
    }

    /**
     * Function using for data updating
     *
     * @param $model
     * @param string $type
     * @return bool
     */
    protected function updateData($model, string $type)
    {
        switch ($type) {
            case 'brand':
                $this->data['product']['brand_id'] = $model->id;
                return true;
                break;
            case 'product':
                $this->data['product_profile']['product_id'] = $model->id;
                return true;
                break;
            case 'productProfile':
                foreach ($this->data['price'] as $key => $price) {
                    $this->data['price'][$key]['product_profile_id'] = $model->id;
                }
                $this->data['link']['product_profile_id'] = $model->id;
                return true;
                break;
            case 'price';
                return true;
                break;
            default:
                return false;
        }
    }

    /**
     * @return mixed
     */
    protected function checkBrand()
    {
        return $this->checker->brand($this->data['brand']);
    }

    /**
     * @return bool
     */
    protected function createBrand()
    {
        $brand = $this->creator->brand($this->data['brand']);
        $this->updateData($brand, 'brand');

        return (boolean) $this->createProduct();
    }

    /**
     * @return mixed
     */
    protected function checkProduct()
    {
        $where = [
            0 => ['check_name', $this->data['product']['check_name']],
            1 => ['brand_id', $this->data['product']['brand_id']],
         ];

        return $this->checker->product($where);
    }

    /**
     * @return bool
     */
    protected function createProduct()
    {
        $product = $this->creator->product($this->data['product']);
        $this->updateData($product, 'product');

        return (boolean) $this->createProductProfile();
    }

    /**
     * @return mixed
     */
    protected function checkProductProfile()
    {
        $where = [
            0 => ['product_id', $this->data['product_profile']['product_id']],
            1 => ['site_id', $this->data['product_profile']['site_id']],
        ];

        return $this->checker->productProfile($where);
    }

    /**
     * @return bool
     */
    protected function createProductProfile()
    {
        $productProfile = $this->creator->productProfile($this->data['product_profile']);
        $this->updateData($productProfile, 'productProfile');
        $this->createLink();
        if ($this->withImages) {
            $this->createImages();
        }

        return (boolean) $this->createPrice();
    }

    /**
     * Creates SellLink entity related to productProfile
     *
     * @return mixed
     */
    protected function createLink()
    {
        return $this->creator->link($this->data['link']);
    }

    /**
     * @return mixed
     */
    protected function checkPrice()
    {
        foreach ($this->data['price'] as $price) {
            $where = [
                0 => ['product_profile_id', $price['product_profile_id']],
                1 => ['created_at', '>=', $price['created_at']],
                2 => ['condition', $price['condition']],
                3 => ['network', $price['network']],
            ];
            if (! $this->checkSinglePrice($where)) {
                return null;
            }
        }

        return true;
    }

    /**
     * @param array $where
     * @return mixed
     */
    protected function checkSinglePrice(array $where)
    {
        return $this->checker->price($where);
    }

    /**
     * @return bool
     */
    protected function createPrice()
    {
        $this->deleteAllProfilePrices();

        foreach ($this->data['price'] as $price) {
            $this->createSinglePrice($price);
        }

        return true;
    }

    /**
     * @param $price
     * @return bool
     */
    protected function createSinglePrice($price)
    {
        return (boolean) $this->creator->price($price);
    }

    /**
     * Delete all prices with the same params from db
     *
     * @return mixed
     */
    protected function deleteAllProfilePrices()
    {
        foreach ($this->data['price'] as $price) {
            $this->deleteSinglePrice($price);
        }
    }

    /**
     * Delete single price form db
     *
     * @param $price
     * @return mixed
     */
    protected function deleteSinglePrice($price)
    {
        $where = [
            0 => ['product_profile_id', $price['product_profile_id']],
            1 => ['condition', $price['condition']],
            2 => ['network', $price['network']],
        ];

        return $this->destroyer->price($where);
    }

    /**
     *
     */
    protected function createImages()
    {
        foreach ($this->data['images'] as $image) {
            $image = $this->setImageParams($image);
            $this->imageService->create($image);
        }

        return;
    }

    /**
     * @param array $image
     * @return array
     */
    protected function setImageParams(array $image)
    {
        $image['product_id'] = $this->data['product_profile']['product_id'];
        $image['brand'] = $this->data['brand']['name'];
        $image['product_name'] = $this->data['product']['name'];

        return $image;
    }

    /**
     * Configure Model Service
     */
    protected function configure()
    {
        $this->setConfigs()->configureChecker()->configureCreator()->configureDestroyer()->updateConfigs();
    }

    /**
     * Configure checker
     *
     * @return $this
     */
    protected function configureChecker()
    {
        $this->checker->setParams($this->configs);

        return $this;

    }

    /**
     * Configure destroyer
     *
     * @return $this
     */
    protected function configureDestroyer()
    {
        $this->destroyer->setParams($this->configs);

        return $this;

    }

    /**
     * Configure creator
     *
     * @return $this
     */
    protected function configureCreator()
    {
        $this->creator->setParams($this->configs);

        return $this;
    }

    /**
     * Configure
     *
     * @return mixed
     */
    protected function setConfigs()
    {
        $this->configs = Config::get("parser.services." . get_class($this));

        return $this;
    }

    /**
     * Remove data from configs array, that was needed for EntityServices configuration,
     * but doesn't need for current class behaviour.
     *
     * @return $this
     */
    protected function updateConfigs()
    {
        array_pop($this->configs);

        return $this;
    }
}