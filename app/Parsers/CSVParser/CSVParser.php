<?php

namespace App\Parsers\CSVParser;

use App\Parsers\Exceptions\ParserException;
use App\Parsers\Parser;

abstract class CSVParser extends Parser
{
    protected function getCSV($link)
    {
        $file = file($link, FILE_IGNORE_NEW_LINES);
        if ( ! $file) {
            throw new ParserException("Can't connect to the {$this->site->name} api link");
        }

        $csv = array_map('str_getcsv', $file);
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        unset($file);

        return $csv;
    }
}