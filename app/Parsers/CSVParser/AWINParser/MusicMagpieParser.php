<?php

namespace App\Parsers\CSVParser\AWINParser;

use App\Models\Product;
use Carbon\Carbon;

class MusicMagpieParser extends AWINParser
{
    /**
     * @var bool
     */
    protected $imagable = true;

    /**
     * @var null
     */
    protected $name = null;

    /**
     * @var string
     */
    protected $zipName = 'magpie.zip';

    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        $this->name = $this->removeFromName($line['product_name'], 'Refurbished / ');
        $filters = $this->getPriceFilters();

        return [
            'brand' => [
                'name' => $line['brand_name'],
            ],
            'product' => [
                'name' => $this->prepareProductName(),
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['merchant_deep_link'],
            ],
            'price' => [
                0 => [
                    'price' => $line['search_price'],
                    'currency' => $line['currency'],
                    'created_at' => Carbon::today(),
                    'condition' => $filters['condition'],
                    'network' => $filters['network'],
                ],
            ],
            'images' => [
                0 => [
                    'url' => $line['aw_image_url'],
                    'product_class' => Product::class,
                ],
            ],
        ];
    }

    protected function getPriceFilters()
    {
        $result = [];
        $result['condition'] = $this->getFilter('condition');
        $result['network'] = $this->getFilter('network');

        return $result;
    }

    protected function getFilter($filter)
    {
        $params = config("filter.parsing.{$this->getName()}");

        foreach ($params[$filter] as $filterItem => $value) {
            if ($this->checkFilter($filterItem)) {
                $this->name = $this->removeFromName($this->name, $filterItem);
                return $value;
            }
        }

        return 1;
    }

    protected function prepareProductName()
    {
        $this->name = trim($this->removeFromName($this->name, '-'));
        return strpos($this->name, 'iPhone') === 0 ? "Apple {$this->name}" : $this->name;
    }

    protected function removeFromName($name, $value)
    {
        return str_replace($value, '', $name);
    }

    protected function checkFilter($filter)
    {
        return (boolean) strpos($this->name, $filter);
    }
}