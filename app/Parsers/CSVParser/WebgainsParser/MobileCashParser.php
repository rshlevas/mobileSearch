<?php

namespace App\Parsers\CSVParser\WebgainsParser;

use App\Models\Product;

class MobileCashParser extends WebgainsParser
{
    /**
     * @var bool
     */
    protected $imagable = false;

    /**
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        return [
            'product' => [
                'name' => $line['product_name'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $this->getProductLink($line['deeplink']),
            ],
            'price' => [
                0 => [
                    'price' => $line['price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 1,
                    'network' => 1,
                ],
            ],
            'images' => [
                0 => [
                    'url' => $line['image_url'],
                    'product_class' => Product::class,
                ],
            ],
        ];
    }

    /**
     * @param string $link
     * @return mixed
     */
    protected function getProductLink(string $link)
    {
        $pattern = '/(?:http[\:\/a-zA-z0-9-_\.]+)/';
        $matches = [];
        preg_match_all($pattern, $link, $matches);

        return $matches[0][1];
    }

}