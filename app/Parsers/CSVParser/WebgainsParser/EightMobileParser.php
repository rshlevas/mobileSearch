<?php

namespace App\Parsers\CSVParser\WebgainsParser;

use App\Models\Product;

class EightMobileParser extends WebgainsParser
{
    protected $imagable = true;

    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        return [
            'brand' => [
                'name' => $line['brand'],
            ],
            'product' => [
                'name' => $this->getProperName($line),
                'model' => $line['model_number'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $this->getProductLink($line['deeplink']),
            ],
            'price' => [
                0 => [
                    'price' => $line['price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 1,
                    'network' => 1,
                ],
                1 => [
                    'price' => $line['non_working_price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 2,
                    'network' => 1,
                ],
            ],
            'images' => [
                0 => [
                    'url' => $line['image_url'],
                    'product_class' => Product::class,
                ],
            ],
        ];
    }

    /**
     * Set proper product name depending on the datafeed section. Because at the middle of
     * csv file, it a little bit changes his structure, that leads to the some occasions
     *
     * @param array $line
     * @return string
     */
    protected function getProperName(array $line)
    {
        return $line['merchant_category'] === 'Mobile phone' ? $line['product_name'] : $this->prepareName($line);
    }

    /**
     * Build name
     *
     * @param array $line
     * @return string
     */
    protected function prepareName(array $line)
    {
        return "{$line['merchant_category']} {$line['product_name']}";
    }

    /**
     * @param string $link
     * @return mixed
     */
    protected function getProductLink(string $link)
    {
        $pattern = '/(?:http[\:\/a-zA-z0-9-_\.]+)/';
        $matches = [];
        preg_match_all($pattern, $link, $matches);

        return $matches[0][1];
    }
}