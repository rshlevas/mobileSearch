<?php

namespace App\Parsers;

use App\Models\Site;
use App\Parsers\Services\ModelService;

/**
 * Class ParserFactory
 * @package App\Parsers
 */
class ParserFactory
{
    /**
     * Function that returns created parser object
     *
     * @param string       $parser       <p>Parser class name</p>
     * @param string       $siteName     <p>Name of the site related to the parser</p>
     * @param ModelService $modelService <p>Model service objects, that need for parser creation</p>
     * @return mixed
     */
    public function create(string $parser, string $siteName, ModelService $modelService)
    {
        return $this->getParser($parser, $this->getSite($siteName), $modelService);
    }

    /**
     * Take related site from DB by its name
     *
     * @param $siteName <p>The name of the site</p>
     * @return mixed
     */
    protected function getSite($siteName)
    {
        return Site::where('parsing_name', $siteName)->get()->first();
    }

    /**
     * Creates parser object with, that name
     *
     * @param string       $parser       <p>Name of the parser class</p>
     * @param Site         $site         <p>Related site entity</p>
     * @param ModelService $modelService <p>Model service entity</p>
     * @return mixed
     */
    protected function getParser(string $parser, Site $site, ModelService $modelService)
    {
        return new $parser($site, $modelService);
    }
}