<?php

namespace App\Providers;

use App\Models\NegativeReport;
use App\Models\ProductPrice;
use App\Observers\NegativeReportObserver;
use App\Observers\ProductPriceObserver;
use App\Services\EntityServices\PaymentEntitiesService;
use App\Services\EntityServices\ReportEntitiesService;
use App\Services\HighChartService;
use App\Services\ImageServices\ProductImageService;
use Corcel\Model\Post;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        NegativeReport::observe(NegativeReportObserver::class);
        ProductPrice::observe(ProductPriceObserver::class);
        $posts = Post::published()->take(3)->orderBy('post_date', 'desc')->get();
        View::share('posts', $posts);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentEntitiesService::class, function ($app) {
            return new PaymentEntitiesService();
        });

        $this->app->bind(ReportEntitiesService::class, function ($app) {
            return new ReportEntitiesService();
        });

        $this->app->bind(HighChartService::class, function ($app) {
           return new HighChartService();
        });

        $this->app->bind(ProductImageService::class, function ($app) {
            return new ProductImageService();
        });
    }
}
