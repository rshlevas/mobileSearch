<?php

namespace App\Services;

use App\Models\Image;
use App\Models\Site;
use Illuminate\Http\UploadedFile;

class SiteEntityService
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var Site
     */
    protected $site;

    /**
     * @param array $data
     * @param null $file
     * @return Site
     */
    public function store(array $data, $file = null)
    {
        $this->configure($data, $file);
        $this->setSite();
        $this->storeData();

        return $this->site;
    }

    /**
     * Store additional data according the settings
     */
    protected function storeData()
    {
        foreach ($this->checkingParams as $param => $method) {
            isset ($this->data[$param]) ? $this->{$method}() : '';
        }
    }

    /**
     * Set site form DB or creates new Entity
     *
     * @return Site
     */
    protected function setSite()
    {
        return $this->site = isset($this->data['siteId']) ? $this->update() : $this->create();

    }

    /**
     * Creates new Site entity
     *
     * @return mixed
     */
    protected function create()
    {
        $this->setParsingName();

        return Site::create($this->data);
    }

    /**
     * Updates site Entity
     *
     * @return Site
     */
    protected function update()
    {
        $site = $this->getSiteById();
        $site->update($this->data);
        $site->payments()->detach();

        return $site;
    }

    /**
     * Get Site entity by id
     *
     * @return mixed
     */
    protected function getSiteById()
    {
        return Site::find($this->data['siteId']);
    }

    /**
     * Configure the class properties
     *
     * @param array $data
     * @param $file
     */
    protected function configure(array $data, $file)
    {
        $this->file = $file;
        $this->data = $data;
    }

    /**
     * Set parsing name property to data
     *
     * @return string
     */
    protected function setParsingName()
    {
        return $this->data['parsing_name'] = $this->getParsingName($this->data['name']);
    }

    /**
     * Generate parsing name according the site name
     *
     * @param $name
     * @return string
     */
    protected function getParsingName($name)
    {
        return implode('_', explode(' ', strtolower($name)));
    }

    /**
     * Store payment relations
     *
     * @return array
     */
    protected function storePayments()
    {
        return $this->site->payments()->sync($this->data['payments'], false);
    }

    /**
     * Store logo relation
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function storeLogo()
    {
        if ($prevLogo = $this->getPreviousLogo()) {
            $prevLogo->delete();
        }

        return $this->site->image()->save($this->createImage());
    }

    /**
     * Gets previous logo
     *
     * @return mixed
     */
    protected function getPreviousLogo()
    {
        return $this->site->image;
    }

    /**
     * Creates image Entity
     *
     * @return Image
     */
    protected function createImage()
    {
        $image = new Image();
        $image->path = $this->file->store('public/images/logos/sites');
        $image->name = $this->file->hashName();

        return $image;
    }

    /**
     * @var array
     */
    protected $checkingParams = [
        'payments' => 'storePayments',
        'logo' => 'storeLogo'
    ];
}