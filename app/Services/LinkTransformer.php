<?php

namespace App\Services;

class LinkTransformer
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string
     */
    protected $xs;

    /**
     * LinkTransformer constructor.
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * Starts link transformation
     *
     * @param $url
     * @return string
     */
    public function run($url)
    {
        return $this->transform($this->encode($url));
    }

    /**
     * Transform link into necessary format
     *
     * @param $url
     * @return string
     */
    protected function transform($url)
    {
        return "{$this->domain}/?id={$this->key}&xs={$this->xs}&url={$url}";
    }

    /**
     * Returns url encoded link
     *
     * @param string $url
     * @return string
     */
    protected function encode(string $url)
    {
        return urlencode($url);
    }

    /**
     * Function that configure class properties according the config file
     */
    protected function configure()
    {
        $params = config('skimlinks');
        foreach ($params as $param => $value) {
            $this->setParam($param, $value);
        }
    }

    /**
     * Set specific param according to its name
     *
     * @param $param
     * @param $value
     * @return mixed
     */
    protected function setParam($param, $value)
    {
        return $this->{$param} = $value;
    }
}