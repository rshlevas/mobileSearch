<?php

namespace App\Services;

use App\Models\Condition;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class FilterSelector
{
    /**
     * @var
     */
    protected $filtersInfo;

    /**
     * FilterSelector constructor.
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * @param Product $product
     * @return array
     */
    public function run(Product $product)
    {
        $product->load(['profiles', 'profiles.prices']);

        return $this->getFilters($this->getPrices($product));

    }

    /**
     * @param $product
     * @return Collection
     */
    protected function getPrices($product)
    {
        $prices = collect([]);
        $profiles = $product->profiles;
        foreach ($profiles as $profile) {
            foreach ($profile->prices as $price) {
                $prices->push($price);
            }
        }

        return $prices;
    }

    /**
     * @param Collection $prices
     * @return array
     */
    protected function getFilters(Collection $prices)
    {
        $filters = [];
        foreach ($this->filtersInfo as $param) {
            $filters[$param['column']] = $this->getFilter($prices, $param['column']);
        }

        return $this->filter($filters);
    }

    /**
     * @param $filters
     * @return array
     */
    protected function filter($filters)
    {
        $result = [];
        foreach ($filters as $name => $filter) {
            foreach ($filter as $item) {
                $result[$name][] = $this->filtersInfo[$name]['filters'][$item];
            }
        }

        return $result;
    }

    /**
     * @param $prices
     * @param $filter
     * @return mixed
     */
    protected function getFilter($prices, $filter)
    {
        return $prices->map(function($price, $key) use ($filter) {
            return $price->{$filter};
        })->unique()->sort()->toArray();
    }

    /**
     *
     */
    protected function configure()
    {
        $this->filtersInfo = config('filter.services.' . get_class($this));
        $this->setConditionParams();
    }

    /**
     * @return array|bool
     */
    protected function setConditionParams()
    {
        $conditions = [];
        foreach ($this->getConditions() as $condition) {
            $conditions[$condition['key']] = $condition;
        }

        return empty($conditions) ? false : $this->filtersInfo['condition']['filters'] = $conditions;
    }

    /**
     * @return mixed
     */
    protected function getConditions()
    {
        return Condition::select(['name', 'key', 'alias'])
            ->where('lang', App::getLocale())
            ->get()
            ->toArray();
    }

}