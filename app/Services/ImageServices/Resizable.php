<?php

namespace App\Services\ImageServices;

use Intervention\Image\Facades\Image as Resizer;

trait Resizable
{
    private function getParams($type)
    {
        return config("image.{$type}");
    }

    protected function resize($path, $thumb = false)
    {
        return $thumb ? $this->resizeByType($path, $this->getParams('thumb')) : $this->resizeByType($path, $this->getParams('general'));
    }

    private function resizeByType($path, $params)
    {
        return Resizer::make($path)->resize($params['width'], $params['height'])->save($path);
    }

}