<?php

namespace App\Services\ImageServices;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductImageService
{
    use Resizable;

    /**
     * Starts the image storing process
     *
     * @param $file
     * @param Model $model
     * @param bool $thumb
     * @return Image
     */
    public function run($file, Model $model, bool $thumb = false)
    {
        $this->removePrevious($model);
        $name = $this->getImageName();
        $directory = $this->getDirectoryName($model);
        $path = $this->getPath($directory, $name);
        $this->store($file, $directory, $name);
        $this->resize($path, $thumb);

        return $this->createImageEntity($path, $name);
    }

    /**
     * Removes previous images of model
     *
     * @param $model
     * @return bool
     */
    protected function removePrevious($model)
    {
        return count($model->images) ? $this->remove($model->images) : true;
    }

    /**
     * Remove single image
     *
     * @param $images
     * @return bool
     */
    protected function remove($images)
    {
        foreach ($images as $image) {
            $image->delete();
        }

        return true;
    }

    /**
     * Stores file at disk
     *
     * @param $file
     * @param $directory
     * @param $name
     * @return mixed
     */
    protected function store($file, $directory, $name)
    {
        return Storage::disk('productImages')->putFileAs($directory, $file, $name);
    }

    /**
     * Generate image name
     *
     * @return string
     */
    protected function getImageName()
    {
        return str_random(10) . '.jpg';
    }

    /**
     * Transform directory path for image
     *
     * @param $product
     * @return string
     */
    protected function getDirectoryName($product)
    {
        return "{$product->brand->name}/" . implode('_', explode(' ', $product->name));
    }

    /**
     * Generate full path for image
     *
     * @param $directory
     * @param $name
     * @return string
     */
    protected function getPath($directory, $name)
    {
        return "storage/images/products/{$directory}/{$name}";
    }

    /**
     * Creates image entity at db
     *
     * @param $path
     * @param $name
     * @return Image
     */
    protected function createImageEntity($path, $name)
    {
        $image = new Image();
        $image->path = $path;
        $image->name = $name;

        return $image;
    }
}