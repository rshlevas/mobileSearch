<?php

namespace App\Services\EntityServices;

use App\Models\Payment;
use App\Models\PaymentTerm;

class PaymentEntitiesService extends BaseEntityService
{
    /**
     * @var array
     */
    protected $models = [
        'term' => PaymentTerm::class,
        'method' => Payment::class,
    ];

    /**
     * @param $type
     * @return mixed
     */
    public function createPaymentByType($type, $data)
    {
        return $this->models[$type]::create($data);
    }
}