<?php

namespace App\Services\EntityServices;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BaseEntityService
{
    protected $models = [];

    protected $relations = [];

    /**
     * Delete models from collection
     *
     * @param Collection $collection
     */
    public function deleteSeveral(Collection $collection)
    {
        foreach ($collection as $enity)
        {
            $this->delete($enity);
        }

        return;
    }

    /**
     * Delete model from DB
     *
     * @param Model $entity
     * @return bool|null
     */
    protected function delete(Model $entity)
    {
        return $entity->delete();
    }

    /**
     * @param $param
     * @param $type
     */
    public function getByParamsAndType($param, $type)
    {
        $result = $this->models[$type]::where($param)->get()->first();

        return $result ? $result : $this->modelNotFoundExeption($type);
    }

    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function getByIdAndType($id, $type)
    {
        return $this->models[$type]::find($id);
    }

    /**
     * @param Model $model
     * @param $type
     * @return mixed
     */
    public function getByRelatedModel(Model $model, $type)
    {
        return $model->{$this->relations[$type]};
    }

    /**
     * @param $type
     * @param $data
     * @return mixed
     */
    public function createEntityByType($type, $data)
    {
        return $this->models[$type]::create($data);
    }

    /**
     * @param Model $model
     * @param $type
     * @return bool
     */
    public function deleteByRelatedModel(Model $model, $type)
    {
        $this->deleteSeveral($this->getByRelatedModel($model, $type));

        return true;
    }

    /**
     * @param int $id
     * @param $type
     * @return mixed
     */
    public function deleteByIdAndType(int $id, $type)
    {
        $report = $this->getByIdAndType($id, $type);

        return $report->delete();
    }

    /**
     * @param $type
     */
    protected function modelNotFoundExeption($type)
    {
        throw new ModelNotFoundException("Can't found {$this->models[$type]}");
    }
}

