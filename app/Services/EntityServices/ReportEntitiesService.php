<?php

namespace App\Services\EntityServices;

use App\Models\NegativeReport;
use App\Models\PositiveReport;
use App\Models\Site;
use Illuminate\Database\Eloquent\Model;

class ReportEntitiesService extends BaseEntityService
{
    /**
     * @var array
     */
    protected $models = [
        'passed' => PositiveReport::class,
        'failed' => NegativeReport::class,
    ];

    /**
     * @var array
     */
    protected $relations = [
        'all' => 'allReports',
        'passed' => 'passedReports',
        'failed' => 'failedReports'
    ];
}