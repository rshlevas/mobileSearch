<?php

namespace App\Console\Commands;

use App\Models\Site;
use App\Parsers\Interfaces\ParserInterface;
use App\Parsers\ParserFactory;
use App\Parsers\Services\ModelService;
use App\Parsers\Services\ParseStatisticService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Mockery\Exception;

class ParseCommand extends Command
{
    /**
     * @var ParserFactory
     */
    private $factory;

    /**
     * @var ModelService
     */
    private $modelService;

    /**
     * @var ParseStatisticService
     */
    private $statisticService;

    /**
     * Configs, that are set at in parser.php config file
     *
     * @var array
     */
    private $configs;

    /**
     * @var array
     */
    private $startedParsers = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Starts parsing';

    /**
     * ParseCommand constructor.
     * @param ParserFactory $factory
     * @param ModelService $modelService
     * @param ParseStatisticService $statisticService
     */
    public function __construct(ParserFactory $factory, ModelService $modelService, ParseStatisticService $statisticService)
    {
        parent::__construct();
        $this->configs = Config::get('parser');
        $this->factory = $factory;
        $this->modelService = $modelService;
        $this->statisticService = $statisticService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parse();
        $this->displayReport();
    }

    /**
     * Form a parsing list
     *
     * @return array
     */
    protected function getParsersList()
    {
        $siteNames = $this->getActiveSitesList();
        $parseList = $this->getAllParseList();

        return $this->filterParseList($siteNames, $parseList);
    }

    /**
     * Remove inactive sites from site parsing list
     *
     * @param array $siteNames
     * @param array $parseList
     * @return array
     */
    protected function filterParseList(array $siteNames, array $parseList)
    {
        $filtered = [];

        foreach ($siteNames as $key => $site)
        {
            isset($parseList[$site['parsing_name']]) ? $filtered[$site['parsing_name']] = $parseList[$site['parsing_name']] : false;
        }

        return $filtered;
    }

    /**
     * Take active sites from DB
     *
     * @return mixed
     */
    protected function getActiveSitesList()
    {
        return Site::select('parsing_name')->where('is_active', 1)->get()->toArray();
    }

    /**
     * Take parsers classes from configs
     *
     * @return mixed
     */
    protected function getAllParseList()
    {
        return $this->configs['parsers'];
    }

    /**
     * @param ParserInterface $parser
     * @param ParseStatisticService $service
     * @param $siteName
     * @return mixed
     */
    protected function getJob(ParserInterface $parser, ParseStatisticService $service, $siteName)
    {
        $job = $this->configs['job'];

        return new $job($parser, $service, $siteName);
    }

    /**
     * @return mixed
     */
    protected function getQueueName()
    {
        return $this->configs['queue'];
    }

    /**
     * @retun void
     */
    protected function parse()
    {
        foreach ($this->getParsersList() as $key => $value)
        {
            $this->runParser($value, $key);
        }

        return;
    }

    /**
     * @param $parserName
     * @param $siteName
     */
    protected function runParser($parserName, $siteName)
    {
        $parser = $this->factory->create($parserName, $siteName, $this->modelService);
        if ($parser) {
            dispatch($this->getJob($parser, $this->statisticService, $siteName))->onQueue($this->getQueueName());
            $this->startedParsers[$siteName] = $parser->getName();
        }
    }

    /**
     * @return string
     */
    protected function displayReport()
    {
        $this->info(count($this->startedParsers) . " parsers pushed into queues:");
        foreach ($this->startedParsers as $site => $parser) {
            $this->displayParser($site, $parser);
        }
    }

    /**
     * @param $site
     * @param $parser
     */
    protected function displayParser($site, $parser)
    {
        $this->info("{$site} used {$parser}");
    }
}
