<?php

namespace App\Observers;

use App\Models\ProductPrice;
use Carbon\Carbon;

class ProductPriceObserver
{
    /**
     * Listening for ProductPrice events
     *
     * @param ProductPrice $price
     */
    public function created(ProductPrice $price)
    {
        $updated = ['updated_at' => Carbon::now()];
        $price->productProfile->product->update($updated);
        $price->productProfile->product->brand->update($updated);
    }
}