<?php

namespace App\Jobs;

use App\Parsers\Interfaces\ParserInterface;
use App\Parsers\Services\ParseStatisticService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ParseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 5;

    /**
     * @var int
     */
    public $timeout = 3600;

    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     * @var ParseStatisticService
     */
    protected $statisticService;

    /**
     * @var string
     */
    protected $siteName;

    /**
     * Create a new job instance
     *
     * ParseJob constructor.
     * @param ParserInterface $parser
     * @param ParseStatisticService $statisticService
     * @param string $siteName
     */
    public function __construct(ParserInterface $parser, ParseStatisticService $statisticService, string $siteName)
    {
        $this->parser = $parser;
        $this->statisticService = $statisticService;
        $this->siteName = $siteName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = $this->parser->run();
        $this->statisticService->setNotFailed();
        $this->statisticService->makeReport($this->siteName, $result);
        $this->parser->__destruct();
    }

    /**
     * @param \Exception $exception
     */
    public function failed(\Exception $exception)
    {
        $this->statisticService->setFailed();
        $this->statisticService->makeReport($this->siteName, ['reason' => $exception->getMessage()]);
        $this->parser->__destruct();
    }
}
