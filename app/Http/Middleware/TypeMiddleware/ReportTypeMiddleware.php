<?php

namespace App\Http\Middleware\TypeMiddleware;

class ReportTypeMiddleware extends TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = ['all', 'passed', 'failed'];
}
