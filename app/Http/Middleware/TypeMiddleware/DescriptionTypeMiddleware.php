<?php

namespace App\Http\Middleware\TypeMiddleware;

class DescriptionTypeMiddleware extends TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = ['site', 'product'];
}
