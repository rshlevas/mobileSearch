<?php

namespace App\Http\Middleware\TypeMiddleware;

use Closure;

class TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = [];

    /**
     * @var string
     */
    protected $redirectRoute = 'admin_main';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! in_array($request->type, $this->validTypes)) {
            return redirect()->route($this->redirectRoute);
        }

        return $next($request);
    }
}
