<?php

namespace App\Http\Middleware\TypeMiddleware;

class ReportTypeDeleteMiddleware extends TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = ['passed', 'failed'];
}
