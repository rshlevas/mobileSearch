<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Traits\Paginable;
use App\Traits\ProductSortable;
use Corcel\Model\Post;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class BrandController extends Controller
{
    use Paginable, ProductSortable;

    /**
     * Brands list
     */
    public function index()
    {
        $brands = Brand::with('image', 'products')->get();

        return view('brands.index', ['brands' =>$brands]);
    }

    /**
     * Single brand product view
     *
     * @param Request $request
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request, Brand $brand)
    {

        $products = Product::with([
            'profiles',
            'profiles.bestPrice',
            'image',
            'imageThumbnail',
            'brand'
        ])->where('brand_id', $brand->id)
            ->get();

        $paginated = $this->paginate($this->sortByPriceDesc($products), $request);

        return view('brands.view', [
            'brand' => $brand,
            'products' => $paginated,
        ]);
    }
}
