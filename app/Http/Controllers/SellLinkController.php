<?php

namespace App\Http\Controllers;

use App\Models\SellLink;
use App\Services\LinkTransformer;

class SellLinkController extends Controller
{
    /**
     * Redirect user out of the site up to the sell link
     *
     * @param LinkTransformer $transformer
     * @param SellLink $link
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirect(LinkTransformer $transformer, SellLink $link)
    {
        $link->updateCount();
        $path = $transformer->run($link->link);

        return redirect($path);
    }
}
