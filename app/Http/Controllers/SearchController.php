<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Traits\Paginable;
use App\Traits\ProductSortable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SearchController extends Controller
{
    use Paginable, ProductSortable;
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(SearchRequest $request)
    {
        $param = $request->input('search');
        $products = Product::with('brand', 'profiles', 'profiles.bestPrice', 'imageThumbnail', 'image')
            ->where('name', 'like', "%{$param}%")
            ->get();

        $paginated = $this->paginate($this->sortByPriceDesc($products), $request);
        $paginated->appends(['search' => $param])->links();


        return view('products.search_result', [
            'products' => $paginated,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function autocomplete(Request $request)
    {
        $param = $request->input('search');

        $products = Product::with('brand', 'profiles', 'imageThumbnail', 'image', 'profiles.bestPrice')
            ->where('name', 'like', "%{$param}%")
            ->limit(16)
            ->get();

        if (! count($products)) {
           $result[0]['message'] = Lang::get('products.search.failed');
           return response()->json(['data' => $result]);
        }

        return ProductResource::collection($products);
    }
}
