<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Site;
use App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser;
use App\Parsers\Services\ModelService;
use App\Services\HighChartService;

class HomeController extends Controller
{

    public function index(HighChartService $chartService)
    {
        $groups = Group::with('sites', 'sites.failedReports', 'sites.passedReports')->get();
        $sites = Site::with('failedReports', 'passedReports')
            ->where('group_id', null)->get();

        return view('admin.main', ['groups' => $groups, 'sitesWithoutGroup' => $sites, 'charts' => $chartService->generate()]);
    }

    public function parse(ModelService $modelService)
    {
        $site = Site::find(9);
        $parser = new CarphoneWarehouseParser($site, $modelService);

        $parser->run();

    }


}
