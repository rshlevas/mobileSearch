<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductImageRequest;
use App\Models\Product;
use App\Services\ImageServices\ProductImageService;

class ProductController extends Controller
{
    /**
     * Render product view
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Product $product)
    {
        $filters = config('filter.product_display');

        return view('admin.products.view', [
           'product' => $product,
            'filters' => $filters,
        ]);
    }

    /**
     * Render list of products without images
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewNoImage()
    {
        $products = Product::doesntHave('image')
            ->paginate(100);

        return view('admin.products.no-image', ['products' => $products]);
    }

    /**
     * Store uploaded product image
     *
     * @param ProductImageRequest $request
     * @param ProductImageService $imageSevice
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductImageRequest $request, ProductImageService $imageSevice, Product $product)
    {
        $image = $imageSevice->run($request->file('image'), $product);
        $imageThumbnail =  $imageSevice->run($request->file('image'), $product, true);
        if ($image && $imageThumbnail) {
            $product->image()->save($image);
            $product->imageThumbnail()->save($imageThumbnail);
        }

        return redirect()->route('admin_product_view', ['product' => $product->url_name]);
    }
}
