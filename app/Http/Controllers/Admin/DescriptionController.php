<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DescriptionFormRequest;
use App\Models\Description;
use App\Services\EntityServices\DescriptionEntityService;
use Illuminate\Http\Request;

class DescriptionController extends Controller
{
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $languages;

    /**
     * @var DescriptionEntityService
     */
    protected $entityService;

    /**
     * DescriptionController constructor.
     * @param DescriptionEntityService $service
     */
    public function __construct(DescriptionEntityService $service)
    {
        $this->entityService = $service;
        $this->languages = config('languages');
    }

    /**
     * Show all descriptions of related model
     *
     * @param $type
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($type, $name)
    {
        $model = $this->entityService->getModelByTypeAndName($type, $name);
        $descriptions = $this->entityService->getByRelatedModel($model, $type);

        return view('admin.descriptions.view', [
            'type' => $type,
            'descriptions' => $descriptions,
            'languages' => $this->languages,
            'model' => $model,

        ]);
    }

    /**
     * Store the description at db
     *
     * @param DescriptionFormRequest $request
     * @param $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DescriptionFormRequest $request, $type)
    {
        $input = $request->input();

        $model = $this->entityService->getByIdAndType($request->relationId, $type);

        if ($request->descriptionId) {
            $description = $this->entityService->getByIdAndType($request->descriptionId, 'description');
            $description->update($input);
        } else {
            $input['describable_id'] = $model->id;
            $input['describable_type'] = get_class($model);
            $description = $this->entityService->createEntityByType('description', $input);
        }

        $name = $type === 'site' ? 'parsing_name' : 'url_name';

        return redirect()->route('admin_descriptions_view', [
            'type' => $type,
            'name' => $model->{$name}
            ]);
    }

    /**
     * Delete description from db
     *
     * @param Description $description
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Description $description)
    {
        $description->delete();

        return redirect()->back();
    }

    /**
     * Render creation form of description entity
     *
     * @param $type
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($type, $model)
    {
        $entity = $this->entityService->getByIdAndType($model, $type);
        $unUsedLanguages = $this->languages;
        if (count($entity->descriptions)) {
            foreach ($entity->descriptions as $description) {
                unset($unUsedLanguages[$description->language]);
            }
        }

        return view('admin.descriptions.create', [
            'type' => $type,
            'languages' => $unUsedLanguages,
            'model' => $entity,
        ]);
    }
}
