<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SiteFormRequest;
use App\Models\Group;
use App\Models\Image;
use App\Models\Payment;
use App\Models\PaymentTerm;
use App\Models\Site;
use App\Services\SiteEntityService;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $groups = Group::with('sites', 'sites.image')->get();
        $sites = Site::with('image')->where('group_id', null)->get();

        return view('admin.sites.index', ['groups' => $groups, 'sitesWithoutGroup' => $sites]);
    }

    /**
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Site $site)
    {
        return view('admin.sites.view', ['site' => $site]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $payments = Payment::All();
        $paymentTerms = PaymentTerm::all();
        $groups = Group::all();

        return view('admin.sites.create', [
            'payments' => $payments,
            'terms' => $paymentTerms,
            'groups' => $groups,
        ]);
    }

    /**
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Site $site)
    {
        $payments = Payment::all();
        $paymentTerms = PaymentTerm::all();
        $groups = Group::all();

        return view('admin.sites.update', [
            'site' => $site,
            'payments' => $payments,
            'terms' => $paymentTerms,
            'groups' => $groups,
        ]);
    }

    /**
     * @param SiteFormRequest $request
     * @param SiteEntityService $siteService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(SiteFormRequest $request, SiteEntityService $siteService)
    {
        $input = $request->all();
        $siteService->store($input, $request->file('logo'));

        return redirect()->route('admin_sites_main');
    }

    /**
     * @param Site $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site)
    {
        $site->delete();

        return redirect()->route('admin_sites_main');
    }
}
