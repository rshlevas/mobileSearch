<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Condition;
use App\Models\Question;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Main quiz section
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $questions = Question::with('answers')
            ->where('lang', 'en')
            ->orderBy('order')
            ->get();

        $conditions = Condition::where('lang', 'en')->orderBy('min_weight')->get();

        return view('admin.quiz.index', [
            'questions' => $questions,
            'conditions' => $conditions,
        ]);
    }

    /**
     * Store the changing in question order
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeQuestionOrder(Request $request)
    {
        foreach (json_decode($request->questions, true) as $params) {
            $question = Question::find($params['id']);
            $question->order = $params['order'];
            $question->save();
            foreach ($question->children as $child) {
                $child->order = $params['order'];
                $child->save();
            }
        }

        return response()->json(['data' => 'Success']);
    }
}
