<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Services\QuizServices\QuestionService;
use Illuminate\Http\Request;

class QuestionTranslationController extends Controller
{
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $languages;

    /**
     * QuestionTranslationController constructor.
     */
    public function __construct()
    {
        $languages = config('languages');
        unset($languages['en']);
        $this->languages = $languages;
    }

    /**
     * Render main translation page for given question
     *
     * @param Question $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Question $question)
    {
        return view('admin.quiz.question.translation.index', [
            'languages' => $this->languages,
            'question' => $question
        ]);
    }

    /**
     * Rendering translation create page
     *
     * @param Question $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Question $question)
    {
        if(count($question->children)) {
            $children = $question->children;
            foreach ($children as $child) {
                unset($this->languages[$child->lang]);
            }
        }

        return empty($this->languages) ? redirect()->back() : view('admin.quiz.question.translation.create', [
            'languages' => $this->languages,
            'question' => $question
        ]);
    }

    /**
     * Rendering translation update page
     *
     * @param Question $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Question $question)
    {
        return view('admin.quiz.question.translation.update', [
            'languages' => $this->languages,
            'question' => $question
        ]);
    }

    /**
     * Saving translation info
     *
     * @param Request $request
     * @param QuestionService $questionService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, QuestionService $questionService)
    {

        $input = $request->input();

        isset($input['questionId'])
            ? $questionService->updateTranslation($input)
            : $questionService->createTranslation($input);

        return redirect()->route('admin_question_translation_view', ['question' => $input['parent_id']]);
    }
}
