<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionCreationRequest;
use App\Models\Question;
use App\Services\QuizServices\QuestionService;

class QuestionController extends Controller
{
    /**
     * Create question section
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.quiz.question.create');
    }

    /**
     * Update question section
     *
     * @param Question $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Question $question)
    {
        return view('admin.quiz.question.update', ['question' => $question]);
    }

    /**
     * Store obtain data
     *
     * @param Question $question
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Question $question)
    {
        $question->delete();

        return redirect()->route('admin_quiz_main');
    }

    /**
     * @param QuestionCreationRequest $request
     * @param QuestionService $questionService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(QuestionCreationRequest $request, QuestionService $questionService)
    {
        $input = $request->input();

        isset($input['questionId']) ? $questionService->update($input) : $questionService->create($input);

        return redirect()->route('admin_quiz_main');
    }
}
