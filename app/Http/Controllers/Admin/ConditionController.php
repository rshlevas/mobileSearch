<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConditionWeightRequest;
use App\Models\Condition;

class ConditionController extends Controller
{
    /**
     * Updates conditions weight parameters
     *
     * @param ConditionWeightRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ConditionWeightRequest $request)
    {
        $conditions = Condition::all();
        foreach ($conditions as $condition) {
            $condition->max_weight = ($request->{"$condition->alias-max-weight"});
            $condition->min_weight = ($request->{"$condition->alias-min-weight"});
            $condition->save();
        }

        return redirect()->route('admin_quiz_main');
    }
}
