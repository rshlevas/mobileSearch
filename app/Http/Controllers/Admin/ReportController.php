<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NegativeReport;
use App\Models\PositiveReport;
use App\Models\Site;
use App\Services\EntityServices\ReportEntitiesService;

class ReportController extends Controller
{
    /**
     * @var ReportEntitiesService
     */
    protected $reportService;

    /**
     * ReportController constructor.
     * @param ReportEntitiesService $reportService
     */
    public function __construct(ReportEntitiesService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * Report view page rendering depending on site and report rype
     *
     * @param Site $site
     * @param $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Site $site, $type)
    {
        $reports = $this->reportService->getByRelatedModel($site, $type);

        return view('admin.reports.view', [
            'reports' => $reports,
            'site'=> $site,
            'type' => $type,
        ]);
    }

    /**
     * Delete ParseReport entity by ID
     *
     * @param int $report
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($report, $type)
    {
        $this->reportService->deleteByIdAndType($report, $type);

        return redirect()->back();
    }

    /**
     * Destroy report of some site by given type
     *
     * @param Site $site
     * @param $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyByType(Site $site, $type)
    {
        $this->reportService->deleteByRelatedModel($site, $type);

        return redirect()->route('admin_main');
    }

    /**
     * Destroy all reports
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyAll()
    {
        $this->reportService->deleteSeveral(PositiveReport::all());
        $this->reportService->deleteSeveral(NegativeReport::all());

        return redirect()->route('admin_main');
    }
}
