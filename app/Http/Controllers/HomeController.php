<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Site;
use App\Traits\Paginable;
use App\Traits\ProductSortable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use ProductSortable, Paginable;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::with('image')
            ->take(18)->get();

        $sites = Site::with('image')
            ->take(12)->get();

        $bestPricesProductsIds = DB::table('product_prices as pr')
            ->select('p.id')
            ->join('product_profile as pp', 'pr.product_profile_id', '=', 'pp.id')
            ->join('products as p', 'pp.product_id', '=', 'p.id')
            ->where(['pr.condition' => 1, 'pr.network' => 1])
            ->orderBy('pr.price', 'desc')
            ->limit(100)
            ->get()
            ->unique();

        $bestPricesProducts = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice', 'brand')
            ->whereIn(
                'id',
                array_slice($bestPricesProductsIds->map(function($pr) {
                    return $pr->id;
                })->toArray(),
                0,
                24))
            ->get();

        $mostPopular = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice')
            ->take(5)
            ->where('is_popular', 1)
            ->get();

        $iphones = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice')
            ->take(5)
            ->where('name', 'like', '%iphone%')
            ->get();

        $samsung = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice')
            ->take(5)
            ->where('name', 'like', '%galaxy%')
            ->get();

        return view('home', [
            'sites' => $sites,
            'brands' => $brands,
            'bestPrices' => $this->sortByPriceDesc($bestPricesProducts)->chunk(8),
            'mostPopular' => $this->sortByPriceDesc($mostPopular),
            'iphones' => $this->sortByPriceDesc($iphones),
            'samsung' => $this->sortByPriceDesc($samsung),
        ]);
    }

    /**
     * Show the most popular products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewPopular(Request $request)
    {
        $mostPopular = Product::with('images', 'imageThumbnail', 'profiles', 'brand', 'profiles.bestPrice')
            ->where('is_popular', 1)
            ->get();

        $paginated = $this->paginate($this->sortByPriceDesc($mostPopular), $request, 20);

        return view('site.most_popular', [
           'products' => $paginated,
        ]);
    }

    /**
     * Show the key products products
     *
     * @param Request $request
     * @param $keyPhone
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function viewKeyProducts(Request $request, $keyPhone)
    {
        $products = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice', 'brand')
            ->where('name', 'like', "%{$keyPhone}%")
            ->get();

        $paginated = $this->paginate($this->sortByPriceDesc($products), $request);

        return view('site.key_products', [
            'products' => $paginated,
            'keyPhone' => $keyPhone,
        ]);
    }

    /**
     * Show the products with the best prices
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewBestPrices()
    {
        $bestPricesProductsIds = DB::table('product_prices as pr')
            ->select('p.id')
            ->join('product_profile as pp', 'pr.product_profile_id', '=', 'pp.id')
            ->join('products as p', 'pp.product_id', '=', 'p.id')
            ->where(['pr.condition' => 1, 'pr.network' => 1])
            ->orderBy('pr.price', 'desc')
            ->limit(100)
            ->get()
            ->unique();

        $bestPricesProducts = Product::with('profiles', 'image', 'imageThumbnail', 'profiles.bestPrice', 'brand')
            ->whereIn(
                'id',
                array_slice($bestPricesProductsIds->map(function($pr) {
                    return $pr->id;
                })->toArray(),
                    0,
                    24))
            ->get();

        return view('site.best_prices', [
            'products' => $this->sortByPriceDesc($bestPricesProducts),
        ]);
    }

    public function viewAbout()
    {
        return view('site.about');
    }
}
