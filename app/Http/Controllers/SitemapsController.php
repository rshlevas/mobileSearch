<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use Watson\Sitemap\Facades\Sitemap;

class SitemapsController extends Controller
{
    /**
     * General Sitemaps file
     *
     * @return mixed
     */
    public function index()
    {
        Sitemap::addSitemap(route('sitemaps_products'));
        Sitemap::addSitemap(route('sitemaps_brands'));

        return Sitemap::index();
    }

    /**
     * SiteMaps for products
     *
     * @return mixed
     */
    public function products()
    {
        $products = Product::all();

        foreach ($products as $product) {
            $tag = Sitemap::addTag(route('product_view', $product->url_name), $product->updated_at, 'daily', '0.8');
            if ($product->image) {
                $tag->addImage(asset($product->image->path));
            }
        }

        return Sitemap::render();
    }

    /**
     * SiteMaps for brands
     *
     * @return mixed
     */
    public function brands()
    {
        $brands = Brand::all();

        foreach ($brands as $brand) {
            $tag = Sitemap::addTag(route('brand_view', $brand->url_name), $brand->updated_at, 'daily', '0.6');
        }

        return Sitemap::render();
    }
}
