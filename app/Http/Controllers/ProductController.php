<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Site;
use App\Services\FilterPriceService;
use App\Services\FilterSelector;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var FilterPriceService
     */
    protected $filterService;

    /**
     * ProductController constructor.
     * @param FilterPriceService $filterService
     */
    public function __construct(FilterPriceService $filterService)
    {
        $this->filterService = $filterService;
    }

    /**
     * Render product view
     *
     * @param FilterSelector $selector
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(FilterSelector $selector, Product $product)
    {
        $prices = $this->filterService->filter($product);
        $filters = $selector->run($product);

        $unknownSites = Site::with('image', 'payments', 'paymentTerm')
            ->where('is_active', 2)->get();

        $bestPrice = $prices->shift();

        return view('products.view', [
            'product' => $product,
            'prices' => $prices,
            'unknownSites' => $unknownSites,
            'bestPrice' => $bestPrice,
            'filters' => $filters,
        ]);
    }

    public function filterBestPrice(Request $request, Product $product)
    {
        $prices = $this->filterService->filter($product, $request->query());
        $bestPrice = $prices->shift();
        if ($bestPrice) {
            $html = view('products.components.best_price', [
                'bestPrice' => $bestPrice,
            ])->render();
        } else {
            $html = '<div></div>';
        }

        return response()->json(['html' => $html]);
    }

    public function filterPrices(Request $request, Product $product)
    {
        $prices = $this->filterService->filter($product, $request->query());
        $bestPrice = $prices->shift();
        $unknownSites = Site::with('image', 'payments', 'paymentTerm')
            ->where('is_active', 2)->get();

        $html = view('products.components.product_site_table', [
            'prices' => $prices,
            'bestPrice' => $bestPrice,
            'unknownSites' => $unknownSites
        ])->render();

        return response()->json(['html' => $html]);
    }

}
