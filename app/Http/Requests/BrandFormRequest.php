<?php

namespace App\Http\Requests;

use App\Models\Brand;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BrandFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:brands',
            'logo' => 'mimes:jpeg,bmp,png|max:1024',
        ];

        $input = $this->all();
        if (isset($input['brand_id'])) {
            $brand = Brand::find($input['brand_id']);
            if ($input['name'] != $brand->name) {
                return $rules;
            } else {
                $rules['name'] = 'required';
            }
        }

        return $rules;
    }
}
