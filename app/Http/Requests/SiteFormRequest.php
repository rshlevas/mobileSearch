<?php

namespace App\Http\Requests;

use App\Models\Site;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SiteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:sites',
            'path' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'trust_point' => 'nullable|numeric|between:0,10',
            'logo' => 'mimes:jpeg,bmp,png|max:1024',
            'trustpilot_link' => 'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ];

        $input = $this->all();
        if (isset($input['siteId'])) {
            $site = Site::find($input['siteId']);
            if ($input['name'] != $site->name) {
                return $rules;
            } else {
                $rules['name'] = 'required';
            }
        }

        return $rules;
    }
}
