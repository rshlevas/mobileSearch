<?php

namespace App\Http\Requests;

use App\Models\Question;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class QuestionCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'content' => 'required|unique:questions',
        ];

        $input = $this->all();
        if (isset($input['questionId'])) {
            $question = Question::find($input['questionId']);
            if ($input['content'] == $question->content) {
                $rules['content'] = 'required';
            }
        }

        return $rules;
    }
}
