<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'name',
        'path',
        'api_link',
        'is_active',
        'is_returnable',
        'payment_term_id',
        'last_parsing',
        'group_id',
        'parsing_name',
        'is_free_post',
        'trust_point',
        'trustpilot_link',
    ];

    /**
     * All products of current site
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(ProductProfile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function payments()
    {
        return $this->belongsToMany(Payment::class, 'payments_sites', 'site_id', 'payment_id');
    }

    /**
     * @return mixed
     */
    public function getPaymentIdsAttribute()
    {
        return $this->payments()->pluck('payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentTerm()
    {
        return $this->belongsTo(PaymentTerm::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        $this->image()->delete();

        return parent::delete();
    }

    /**
     * Get failed parser reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function failedReports()
    {
        return $this->hasMany(NegativeReport::class)
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get passed parser reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passedReports()
    {
        return $this->hasMany(PositiveReport::class)
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get all parser reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getAllReportsAttribute()
    {
        $failed = $this->failedReports;
        $passed = $this->passedReports;
        $combine = $failed->concat($passed);
        return $combine->sortByDesc(function ($report) {
            return $report->created_at;
        });
    }

    /**
     * Group related to site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Transform trust point attribute in star numbers
     *
     * @return int
     */
    public function getStarsNumberAttribute()
    {
        return round($this->trust_point / 2) == 0.0 ? 1 : (int) round($this->trust_point / 2);
    }

    /**
     * Name of the route key
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'parsing_name';
    }

    /**
     * Return description of site
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function descriptions()
    {
        return $this->morphMany(Description::class, 'describable');
    }

    /**
     * Return description of site at given language
     *
     * @return Description
     */
    public function description($lang)
    {
        return $this->morphMany(Description::class, 'describable')
            ->where('language', $lang)
            ->get()
            ->first();
    }
}
