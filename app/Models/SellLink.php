<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellLink extends Model
{
    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'sell_links';

    /**
     * Fillable columns
     *
     * @var array
     */
    protected $fillable = ['link', 'product_profile_id', 'count'];

    /**
     * No timestamp columns
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Product profile related to this link
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(SellLink::class, 'product_profile_id', 'id');
    }

    /**
     * Update count when user go up to link
     *
     * @return bool
     */
    public function updateCount()
    {
        $this->count++;

        return $this->update(['count' => $this->count]);
    }


}
