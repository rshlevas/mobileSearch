<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'model', 'brand_id', 'check_name', 'url_name', 'is_popular', 'updated_at',
    ];

    /**
     * Brand name of current product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Single product profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function singleProfile()
    {
        return $this->hasOne(ProductProfile::class);
    }
    /**
     * ProductProfiles of current product with last prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profiles()
    {
        return $this->hasMany(ProductProfile::class);
    }

    /**
     * ProductProfiles of current product all prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profilesWithAllPrices()
    {
        return $this->hasMany(ProductProfile::class)->with('prices');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function allImages()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable')->where('is_thumbnail', 0);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function imageThumbnail()
    {
        return $this->morphOne(Image::class, 'imagable')->where('is_thumbnail', 1);
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        foreach ($this->allImages() as $image) {
            $image->delete();
        }

        return parent::delete();
    }

    /**
     * Return the name of url model key
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }

    /**
     * @return mixed|string
     */
    public function getShortNameAttribute()
    {
        return $this->getShorter('name');
    }

    /**
     * @return mixed|string
     */
    public function getShortModelAttribute()
    {
        return $this->getShorter('model');
    }

    /**
     * @param $type
     * @return mixed|string
     */
    protected function getShorter($type)
    {
        return strlen($this->{$type}) > 28 ? substr($this->{$type}, 0, 25) . '...' : $this->{$type};
    }

    /**
     * Returns best price of current product among all profiles
     *
     * @return mixed
     */
    public function getBestPriceAttribute()
    {
        $prices = collect([]);

        foreach ($this->profiles as $profile) {
            $prices->push($profile->bestPrice);
        }
        $sorted = $prices->sortByDesc(function($price) {
            return $price->price;
        });

        return $sorted->first();
    }

    /**
     * Return number of profiles
     *
     * @return int
     */
    public function getProfilesCountAttribute()
    {
        return count($this->profiles);
    }

    /**
     * Return descriptions of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function descriptions()
    {
        return $this->morphMany(Description::class, 'describable');
    }
}
