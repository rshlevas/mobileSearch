<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProfile extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_profile';

    /**
     * @var array
     */
    protected $fillable = [
        'product_id', 'site_id', 'link'
    ];

    /**
     * Sites related to this product entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Base product entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Working price of current product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function price()
    {
        return $this->hasOne(ProductPrice::class)
            ->where('condition', 1);
    }

    /**
     * Prices of current product entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bestPrice()
    {
        return $this->hasOne(ProductPrice::class)
            ->orderBy('price', 'desc');
    }

    /**
     * Sell link, that redirects to owner site
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function link()
    {
        return $this->hasOne(SellLink::class);
    }
}
