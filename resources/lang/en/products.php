<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'click.sell' => 'Click to sell',
    'compare' => 'We compare all the leading buyers, so you get the guaranteed best price for your :product',
    'compare.prices.several' => 'Compare :number prices',
    'compare.prices.single' => 'Check the price',
    'filter.condition' => 'Condition',
    'filter.network' => 'Network',
    'filter.unlocked' => 'Unlocked',
    'model' => 'Model',
    'most-popular.title' => 'Compare and sell phones among the most popular items',
    'no-prices' => 'Sorry, but there is no info about current product',
    'price' => 'Price',
    'price.unavailable' => 'Price is unavailable now. Check site for more info',
    'search.failed' => 'We couldn\'t find any phone matching your search',
    'search.phrase' => 'We found :number phones for you',
    'search.result' => 'Search result',
    'site' => 'Site',
    'site.trust' => 'Review',
    'title' => 'Sell :product for up to :price',
    'top.price' => 'Top Price Paid',
    'top.price.table.header' => 'Top Price For :product',
];