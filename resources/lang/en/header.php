<?php
 return [
     /*
      |--------------------------------------------------------------------------
      | Header Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the header.
      |
      */

     'brand' => 'Brands',
     'home' => 'Home',
     'most.popular' => 'Most Popular',
     'partners' => 'Partners',
     'prices' => 'Best prices',
     'search.placeholder' => 'Enter your phone',
     'works' => 'How its works',
 ];