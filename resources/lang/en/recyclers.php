<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'about' => 'About :site',
    'free.post' => 'Free Post?',
    'method' => 'Payment methods',
    'no.rating' => 'No Rating Available!',
    'return' => 'Free return?',
    'term' => 'Speed of payment',
    'title.index' => 'We cooperate with...',
    'title.view' => 'Recyclers: About :site',
    'trust' => 'Trustpilot Rating',
    'visit' => 'Visit the :site website.',
    'website' => 'Recycler Website',


];