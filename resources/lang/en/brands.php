<?php

return [
    'brand' => 'Brand',
    'phrase.index' => 'We have :number brands for you',
    'phrase.view' => 'We have :number phones of this brand',
    'products.count' => 'View all :count products',
    'products.count.single' => 'Check the product',
    'title' => 'Compare and sell phones from :brand',
];