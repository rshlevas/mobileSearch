<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homepage Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by homepage of the application.
    |
    |
    */
    'best.prices' => 'Best prices',
    'best.prices.phrase' => 'Sell your phone by the best price',
    'brands' => 'You can find phones of the most popular brands here',
    'most.popular' => 'Most popular',
    'most.popular.phrase' => 'Most Popular Traded In Phones',
    'partners' => 'We cooperate with',
    'works' => 'How its works',
];