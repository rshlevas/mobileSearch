<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the different buttons.
    | Such as: search, view more, etc.
    |
    */
    'check.site' => 'Check Site',
    'filter.damaged' => 'Damaged',
    'filter.not.working' => 'Not working',
    'filter.working' => 'Working',
    'search' => 'Search',
    'sell' => 'Sell',
    'sell.now' => 'Sell Now',
    'view.more' => 'View More',


];