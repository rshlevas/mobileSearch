@extends('layouts.app')

@section('title', trans('recyclers.title.index'))

@section('content')
    <div class="container">
        <div class="row">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('header.home')</a>
                </li>
                <li class="active text-capitalize"><strong>@lang('header.partners')</strong></li>
            </ul>
            @foreach($sites as $site)
                @include('recyclers.recycler_info', ['site' => $site])
            @endforeach
            <br>
            @foreach($sites as $site)
                @include('recyclers.recycler_description', ['site' => $site])
            @endforeach
        </div>
    </div>
@endsection