@extends('layouts.app')

@section('title', trans('recyclers.title.view', ['site' => $site->name]))

@section('content')
    <div class="container">
        <div class="row">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('header.home')</a>
                </li>
                <li>
                    <a href="{{ route('recyclers_list') }}">@lang('header.partners')</a>
                </li>
                <li class="active text-capitalize"><strong>{{ $site->name }}</strong></li>
            </ul>
            @include('recyclers.recycler_info', ['site' => $site])
            <br>
            @include('recyclers.recycler_description', ['site' => $site])
        </div>
    </div>
@endsection