<div class="col-md-10 col-md-offset-1">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <a href="{{$site->path}}">
            @if ($site->image)
                <img style="max-height: 120px;"
                     src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}"
                     class="img-responsive img-thumbnail">
            @else
                <img style="max-height: 120px;" src="{{ asset('images/no-image.jpeg') }}"
                     class="img img-thumbnail">
        @endif
        </a>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <a class="link-unstyled" href="{{ route('recyclers_view', ['recycler' => $site->parsing_name]) }}">
            <h2>{{ $site->name }}</h2>
        </a>
        <p><a class="btn btn-primary" href="{{ $site->path }}">@lang('recyclers.visit', ['site' => $site->name])
                <i class="icon-external-link"></i></a></p>

    </div>
    <div class="col-md-6">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <th class="col-md-4"></th>
                    <th class="col-md-6"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>@lang('recyclers.trust'):</td>
                    <td>@if ($site->trust_point and $site->trust_point != 0.00)
                            <a href="{{ $site->trustpilot_link }}"><img src="{{ asset("images/{$site->stars_number}-stars.png") }}"
                                    style="height: 20px" class="img img-responsive"></a>
                        @else
                            <i class="icon-fixed-width icon-ban-circle"
                               style="color:red"></i> @lang('recyclers.no.rating')
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('recyclers.website'):</td>
                    <td><a href="{{ $site->path }}">{{ $site->path }}</a></td>
                </tr>
                <tr>
                    <td>@lang('recyclers.term'):</td>
                    <td>
                        @if($site->paymentTerm)
                            {{ $site->paymentTerm->name }}
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('recyclers.method'):</td>
                    <td>
                        @if( count($site->payments) > 0)
                            @foreach($site->payments as $payment)
                                / {{ $payment->name }}
                            @endforeach
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('recyclers.return'):</td>
                    <td>
                        @if ($site->is_returnable == 1)
                            <i class="icon-li icon-ok" style="color:green"></i>
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('recyclers.free.post'):</td>
                    <td>
                        @if ($site->is_free_post == 1)
                            <i class="icon-li icon-ok" style="color:green"></i>
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>