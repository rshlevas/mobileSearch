@extends('layouts.app')

@section('title', trans('brands.title', ['brand' => ucfirst($keyPhone)]))

@section('content')
    <div class="container">
        <div class="row">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('header.home')</a>
                </li>
                <li class="active text-capitalize"><strong>{{ ucfirst($keyPhone) }}</strong></li>
            </ul>
            <div class="col-md-10 col-md-offset-1 text-center">
                <h4>@lang('brands.phrase.view', ['number' => $products->total()])</h4><br>
            </div>
            @include('products.product_list', ['products' => $products])
        </div>
    </div>
@endsection