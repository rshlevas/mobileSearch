@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Popular Products</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h3 class="text-center">Popular products section managing</h3>
                        <a href="{{ route('admin_search') }}" class="btn btn-primary">+ Make new product popular</a>
                        <table class="table table-hover text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th class="col-md-2 info text-center">Image</th>
                                <th class="col-md-3 info text-center">Name</th>
                                <th class="col-md-2 info text-center">Brand</th>
                                <th class="col-md-2 info text-center">Sites Number</th>
                                <th class="col-md-2 info text-center" colspan="2">Manage <i
                                            class="icon-fixed-width icon-cogs"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                    <tr>
                                        <td>
                                            @if ($product->imageThumbnail)
                                                <img style="max-height: 100px;" class="img-responsive center-block"
                                                     src="{{ asset($product->imageThumbnail->path) }}">
                                            @elseif ($product->image)
                                                <img style="max-height: 100px" class="img-responsive center-block"
                                                     src="{{ asset($product->image->path) }}">
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td><a href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">
                                                {{ $product->name }}
                                            </a>
                                        </td>
                                        <td>{{ $product->brand->name }}</td>
                                        <td>{{ count($product->profiles()) }}</td>
                                        <td>
                                            <a class="btn btn-default"
                                               href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">
                                                <i class="icon-fixed-width icon-eye-open"></i>
                                            </a>
                                            <a class="btn btn-danger"
                                               href="#" data-toggle="modal" data-target="#confirm-delete"
                                               data-href="{{ route('admin_remove_popular', ['product' => $product->url_name]) }}">
                                                <i class="icon-trash icon-large"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="modal fade" id="confirm-delete"
                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            @include('admin.components.modal_window', ['target' => 'popularity checkbox'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection