<h3 class="text-center">Conditions value</h3>
<form method="POST" action="{{ route('admin_condition_weight_store') }}">
    {{ csrf_field() }}
    <table class="table table-hover text-center">
        <thead class="text-uppercase">
        <tr>
            <th class="col-md-1 info text-center">№</th>
            <th class="col-md-7 info text-center">Condition</th>
            <th class="col-md-2 info text-center">Min Weight(%)</th>
            <th class="col-md-2 info text-center">Max Weight(%)</th>
        </tr>
        </thead>
        <tbody>
        @php $number = 1; @endphp
        @foreach($conditions as $condition)
            <tr>
                <td>{{ $number }}</td>
                <td>{{ $condition->name }}</td>
                <td>
                    <input type="number" class="form-control number-input" name="{{ $condition->alias }}-min-weight"
                           value="{{ $condition->min_weight }}" required>
                    @if ($errors->has("$condition->alias-min-weight"))
                        <span class="help-block">
                            <strong>{{ $errors->first("$condition->alias-min-weight") }}</strong>
                        </span>
                    @endif
                </td>
                <td>
                    <input type="number" class="form-control number-input" name="{{ $condition->alias }}-max-weight"
                           value="{{ $condition->max_weight }}" required>
                    @if ($errors->has("$condition->alias-max-weight"))
                        <span class="help-block">
                            <strong>{{ $errors->first("$condition->alias-max-weight") }}</strong>
                        </span>
                    @endif
                </td>
            </tr>
            @php $number++; @endphp
        @endforeach
        </tbody>
    </table>
    <button type="submit" class="btn btn-primary">
        <i class="icon icon-save"></i> Save
    </button>
</form>