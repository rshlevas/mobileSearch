<form class="form-horizontal" method="POST" action="{{ route('admin_question_store') }}">
    {{ csrf_field() }}
    <input name="questionId" type="hidden" value="{{ $question->id }}">
    <input class="answer_number" id="answer_number" name="answer_number" type="hidden" value="{{ count($question->answers) }}">
    <div class="form-group">
        <label for="content" class="col-md-4 control-label">Content</label>
        <div class="col-md-6">
            <input id="content" type="text" class="form-control" name="content"
                   value="{{ $question->content }}" required>
            @if ($errors->has('content'))
                <span class="help-block">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div id="answers_section">
        @php $number = 1; @endphp
        @foreach($question->answers as $answer)
            <div class="answer-element">
                <h4 class="text-center">Answer
                    <a href="{{ route('admin_answer_delete', ['answer' => $answer->id]) }}">
                        <i class="icon-remove"></i>
                    </a>
                </h4>
                <input type="hidden" name="answer-id-{{ $number }}" value="{{ $answer->id }}" class="answer-id">
                <div class="form-group">
                    <label for="answer-{{ $number }}"
                           class="col-md-4 control-label label-content">Content</label>
                    <div class="col-md-6">
                        <input id="answer-{{ $number }}" type="text" class="form-control input-content"
                               name="answer-{{ $number }}"
                               value="{{ $answer->content }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer-weight-{{ $number }}"
                           class="col-md-4 control-label label-weight">Weight</label>
                    <div class="col-md-6">
                        <input id="answer-weight-{{ $number }}"
                               type="number" class="form-control input-weight"
                               name="answer-weight-{{ $number }}"
                               value="{{ $answer->weight }}" required>
                    </div>
                </div>
            </div>
            @php $number++; @endphp
        @endforeach
    </div>
    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Update
            </button>
            <button class="btn btn-success add_form_field">
                + Add Answer
            </button>
        </div>
    </div>
</form>