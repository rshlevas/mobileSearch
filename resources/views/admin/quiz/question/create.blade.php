@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_quiz_main') }}">Quiz</a></li>
                            <li class="active text-capitalize">Create Question</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the question</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_question_store') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="content" class="col-md-4 control-label">Content</label>
                                <div class="col-md-6">
                                    <input id="content" type="text" class="form-control" name="content"
                                           placeholder="Enter question content" required>
                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('content') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div id="answers_section">
                                @for ($i = 1; $i < 3; $i++)
                                <div class="answer-element">
                                    <h4 class="text-center"> Answer</h4>
                                    <div class="form-group">
                                        <label for="answer-{{ $i }}" class="col-md-4 control-label label-content">Content</label>
                                        <div class="col-md-6">
                                            <input id="answer-{{ $i }}" type="text" class="form-control input-content" name="answer-{{ $i }}"
                                                   placeholder="Enter answer content" required>
                                        </div>
                                    </div>
                                    <div class="form-group weight">
                                        <label for="answer-weight-{{ $i }}" class="col-md-4 control-label label-weight">Weight</label>
                                        <div class="col-md-6">
                                            <input id="answer-weight-{{ $i }}" type="number" class="form-control input-weight"
                                                   name="answer-weight-{{ $i }}"
                                                   value="0" required>
                                        </div>
                                    </div>
                                </div>
                                @endfor
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                    <button class="btn btn-success add_form_field">
                                        + Add Answer
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var max_fields = 5;
            var wrapper = $("#answers_section");
            var add_button = $(".add_form_field");

            var answerNumber = 2;
            var x = 1;
            getAnswerInput = function (number) {
                var innerHtml = '<div class="answer-element"><h4 class="text-center">Answer' +
                    ' <a href="#" class="delete"><i class="icon-remove"></i></a></h4>' +
                    '<div class="form-group ">' +
                    '<label for="answer-' + number + '" class="col-md-4 control-label label-content">Content' +
                    '</label><div class="col-md-6">' +
                    '<input id="answer-' + number + '" type="text" class="form-control input-content" name="answer-' +
                    number + '" placeholder="Enter answer" required>' +
                    '</div></div><div class="form-group">' +
                    '<label for="answer-weight-' + number + '" class="col-md-4 control-label label-weight">' +
                    'Weight</label><div class="col-md-6"><input id="answer-weight-' + number +
                    '" type="number" class="form-control input-weight" name="answer-weight-' + number +
                    '" value="0" required></div></div>' +
                    '</div>';

                return innerHtml;
            };

            renameAnswerElement = function(length) {
                $('#answers_section').find('.answer-element').each(function (key, el) {
                    var order = key + 1;
                    $(el).find('.label-content').attr('for', 'answer-' + order);
                    $(el).find('.input-content').attr('id', 'answer-' + order);
                    $(el).find('.input-content').attr('name', 'answer-' + order);
                    $(el).find('.label-weight').attr('for', 'answer-weight-' + order);
                    $(el).find('.input-weight').attr('id', 'answer-weight-' + order);
                    $(el).find('.input-weight').attr('name', 'answer-weight-' + order);
                });
            };
            $(add_button).click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    answerNumber++;
                    $(wrapper).append(getAnswerInput(answerNumber)); //add input box
                }
                else {
                    alert('You Reached the limits')
                }
            });

            $(wrapper).on("click", ".delete", function (e) {
                e.preventDefault();
                $(this).parent('h4').parent('div').remove();
                var count = $('#answers_section').find('.answer-element').length;
                renameAnswerElement(count);
                x--;
                answerNumber--;
            })
        });
    </script>
@endsection