@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_quiz_main') }}">Quiz</a></li>
                            <li>
                                <a href="{{ route('admin_question_update', ['question' => $question->id]) }}">
                                    Question №{{ $question->order }} updating
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin_question_translation_view', ['question' => $question->id]) }}">
                                    Translation
                                </a>
                            </li>
                            <li class="active">Create</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Change the native data on translated data</h4>
                        <div class="col-md-12">
                            @include('admin.quiz.question.translation.form', [
                                'languages' => $languages,
                                'question' => $question,
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection