@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_quiz_main') }}">Quiz</a></li>
                            <li>
                                <a href="{{ route('admin_question_update', ['question' => $question->id]) }}">
                                    Question №{{ $question->order }} updating
                                </a>
                            </li>
                            <li class="active">Translation</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Descriptions</h4>
                        @if(count($languages) !== count($question->children))
                            <a class="btn btn-primary"
                               href="{{ route('admin_question_translation_create' ,[
                                    'question' => $question->id
                               ]) }}">
                                + Add Translation
                            </a>
                        @endif
                        <br>
                        <br>
                        @foreach($question->children as $translation)
                            <div class="col-md-12">
                                <div class="col-md-2 control-label">
                                    <p><i class="icon-flag"></i> {{ $languages[$translation->lang] }}</p>
                                    <br>
                                    <a class="btn btn-primary" href="{{ route('admin_question_translation_update', [
                                        'question' => $translation->id
                                    ]) }}">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <a class="btn btn-danger"
                                    href="#" data-toggle="modal" data-target="#confirm-delete"
                                    data-href="{{ route('admin_question_delete', ['question' => $translation->id]) }}">
                                    <i class="icon-trash icon-large"></i>
                                    </a>


                                </div>
                                <div class="col-md-10 well">
                                    <strong>Question:</strong>
                                    {{ $translation->content }}<br>
                                    Answers:<br>
                                    @php $number = 1; @endphp
                                    @foreach($translation->answers as $answer)
                                        {{ $number }}) {{ $answer->content }}<br>
                                        @php $number++; @endphp
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete"
                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                @include('admin.components.modal_window', ['target' => 'these translation'])
            </div>
        </div>
    </div>
@endsection