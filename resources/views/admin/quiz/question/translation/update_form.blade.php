<form class="form-horizontal" method="POST" action="{{ route('admin_question_translation_store') }}">
    {{ csrf_field() }}
    <input name="parent_id" type="hidden" value="{{ $question->parent->id }}">
    <input name="lang" type="hidden" value="{{ $question->lang }}">
    <input name="questionId" type="hidden" value="{{ $question->id }}">
    <input id="answer_number" name="answer_number" type="hidden" value="{{ count($question->answers) }}">
    <div class="form-group">
        <label for="lang" class="col-md-4 control-label"><i class="icon icon-flag"></i> Language</label>
        <div class="col-md-6">
            <select name="lang" class="form-control selectpicker" data-style="btn-primary" id="lang"
                    disabled>
                <option value="{{ $question->lang }}">{{ $languages[$question->lang] }}</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="content" class="col-md-4 control-label">Content</label>
        <div class="col-md-6">
            <input id="content" type="text" class="form-control" name="content"
                   value="{{ $question->content }}" required>
            @if ($errors->has('content'))
                <span class="help-block">
                                            <strong>{{ $errors->first('content') }}</strong>
                                            </span>
            @endif
        </div>
    </div>
    <div id="answers_section">
        @php $number = 1; @endphp
        @foreach($question->answers as $answer)
            <div class="answer-element">
                <input type="hidden" name="answer-id-{{ $number }}" value="{{ $answer->id }}" class="answer-id">
                <h4 class="text-center">Answer
                </h4>
                <div class="form-group">
                    <label for="answer-{{ $number }}"
                           class="col-md-4 control-label label-content">Content</label>
                    <div class="col-md-6">
                        <input id="answer-{{ $number }}" type="text" class="form-control input-content"
                               name="answer-{{ $number }}"
                               value="{{ $answer->content }}" required>
                    </div>
                </div>
                <input type="hidden" name="answer-weight-{{ $number }}" value="{{ $answer->weight }}">
            </div>
            @php $number++; @endphp
        @endforeach
    </div>
    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
</form>