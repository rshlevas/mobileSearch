<table class="table table-hover text-center">
    <thead class="text-uppercase">
    <tr>
        <th class="col-md-2 info text-center">Date</th>
        <th class="col-md-8 info text-center">Reason</th>
        <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td><strong>{{ $report->created_at }}</strong></td>
            <td class="text-left">{{ $report->reason }}</td>
            <td><a href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin_reports_delete_single', ['report' => $report->id, 'type' => $report->type]) }}"
                   class="btn btn-danger">
                    <i class="icon-trash icon-large"></i> Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>