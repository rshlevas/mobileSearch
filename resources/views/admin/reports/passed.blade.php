<table class="table table-hover text-center">
    <thead class="text-uppercase">
    <tr>
        <th class="col-md-2 info text-center">Date</th>
        <th colspan class="col-md-1 info text-center">Brands</th>
        <th colspan class="col-md-1 info text-center">Base products</th>
        <th colspan class="col-md-1 info text-center">Products</th>
        <th colspan class="col-md-1 info text-center">Prices</th>
        <th colspan class="col-md-2 info text-center">Not changed products</th>
        <th colspan class="col-md-2 info text-center">Failed line reading</th>
        <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td><strong>{{ $report->created_at }}</strong></td>
            <td>{{ $report->brand }}</td>
            <td>{{ $report->product }}</td>
            <td>{{ $report->productProfile }}</td>
            <td>{{ $report->price }}</td>
            <td>{{ $report->nonChanged }}</td>
            <td>{{ $report->failedLines }}</td>
            <td><a href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin_reports_delete_single', ['report' => $report->id, 'type' => $report->type]) }}"
                   class="btn btn-danger">
                    <i class="icon-trash icon-large"></i> Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>