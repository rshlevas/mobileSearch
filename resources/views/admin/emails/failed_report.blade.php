@component('mail::message')
    <h3>Parsing failed!!!</h3>
    Site: {{ $report->site->name }}
    Group: {{ $report->site->group->name }}
    Date: {{ $report->created_at }}
    Reason: {{ $report->reason }}
    Please click on the link below for more details.
@component('mail::button', ['url' => route('admin_reports_view', ['site' => $report->site->parsing_name, 'type' => 'failed']), 'color' => 'green'])
    View Reports
@endcomponent
    Thanks,
    {{ config('app.name') }}
@endcomponent