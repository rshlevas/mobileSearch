@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_brands_view') }}">Brands</a></li>
                            <li class="active text-capitalize">Update Brand: <strong>{{ $brand->name }}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the brand params you want to update</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_brands_storage') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $brand->id }}" name="brand_id">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $brand->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label for="logo" class="col-md-4 control-label">Logo</label>

                                <div class="col-md-6">
                                    <div class="col-md-6">
                                    @if ($brand->image)
                                       <img id="logo-image" src="{{ asset('storage/images/logos/brands') }}/{{$brand->image->name}}" class="img img-thumbnail">
                                    @else
                                       <img id="logo-image" src="{{ asset('images/no-image.jpeg') }}" class="img img-thumbnail">
                                    @endif
                                    </div>

                                    <label class="btn btn-default btn-primary">
                                        Browse <input id="logo" type="file" class="form-control file" name="logo" style="display: none;">
                                    </label>
                                    <span class="image-name"></span>
                                    @if ($errors->has('logo'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('logo') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection