@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li>
                                <a href="
                                @if($type === 'site')
                                    {{ route('admin_sites_view', ['site' => $model->parsing_name]) }}
                                @elseif($type === 'product')
                                    {{ route('admin_product_view', ['product' => $model->url_name]) }}
                                @endif
                                ">
                                    {{ $model->name }}
                                </a>
                            </li>
                            <li class="active">Descriptions</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Descriptions</h4>
                        @if(count($languages) !== count($descriptions))
                            <a id="add_description" class="btn btn-primary"
                               href="{{ route('admin_descriptions_create' ,[
                                    'type' => $type,
                                    'model' => $model->id
                               ]) }}">
                                + Add description
                            </a>
                        @endif
                        <br>
                        <br>
                        @foreach($descriptions as $description)
                            <div class="col-md-12 view-{{ $description->id }}">
                                <div class="col-md-2 control-label">
                                    <p><i class="icon-flag"></i> {{ $languages[$description->language] }}</p>
                                    <br>
                                    <button class="btn btn-primary edit"
                                            data-id="{{ $description->id }}"
                                            type="button">
                                        <i class="icon-pencil"></i> Edit
                                    </button>

                                </div>
                                <div class="col-md-10 well">
                                    {!! $description->content !!} <br>

                                </div>
                            </div>
                            <div class="col-md-12 edit-{{ $description->id }}" style="display: none">
                                @include('admin.descriptions.form', [
                                    'languages' => $languages,
                                    'model' => $model,
                                    'description' => $description
                                ])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete"
                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                @include('admin.components.modal_window', ['target' => 'these description'])
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script>
        $(".edit").click(function () {
            var id = $(this).attr("data-id");
            $(".view-" + id).slideToggle( "fast" );
            $(".edit-" + id).slideToggle( "fast" );

            return false;
        });

        $(".cancel").click(function () {
            var id = $(this).attr("data-id");
            $(".edit-" + id).slideToggle( "fast" );
            $(".view-" + id).slideToggle( "fast" );

            return false;
        });

        $(document).ready(function() {

            $('.summernote').summernote({
                height: 150
            });
        });

    </script>
@endsection