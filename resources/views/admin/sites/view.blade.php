@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_sites_main') }}">Sites</a></li>
                            <li class="active">Site: <strong>{{ $site->name }}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">{{ $site->name }}</h4><br>
                        <div class="col-md-4">
                            @if ($site->image)
                                <img style="max-height: 200px;"
                                     src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}"
                                     class="img-responsive img-thumbnail">
                            @else
                                <img style="max-height: 200px;" src="{{ asset('images/no-image.jpeg') }}"
                                     class="img img-thumbnail">
                            @endif
                        </div>
                        <div class="col-md-8">
                            <p>Link - <strong>{{ $site->path }}</strong></p>
                            <p><i class="icon-money"></i> Payment types - @if($site->payments)
                                    @foreach($site->payments as $payment)
                                        <strong>{{$payment->name}}</strong>,
                                    @endforeach
                                @endif
                            </p>
                            <p><i class="icon-group"></i> Group: <strong>{{ isset($site->group->name) ? $site->group->name : 'Without group' }}</strong>
                            <p>
                                <i class="icon-calendar"></i> Payment term:
                                <strong>{{ $site->paymentTerm ? $site->paymentTerm->name : ''}}</strong>
                            </p>
                            <p>Active - <strong>{{ $site->is_active === 1 ? 'Yes' : ($site->is_active === 0 ? 'No' : 'Unknown') }}</strong></p>
                            <p>Returnable - <strong> {{ $site->is_returnable == 1 ? 'Yes' : 'No' }}</strong></p>
                            <p>Free Post - <strong> {{ $site->is_free_post == 1 ? 'Yes' : 'No' }}</strong></p>
                            <p>Trust Rating - <strong> {{ $site->trust_point }}</strong></p>
                            <p>Number of products - <strong>{{ count($site->products) }}</strong></p>
                            <p>Number of
                                <a href="{{ route('admin_reports_view', ['site' => $site->parsing_name,'type' => 'failed']) }}">
                                    failed
                                </a> reports</a> - <strong>{{ count($site->failedReports) }}</strong></p>
                            <p>Number of <a href="{{ route('admin_reports_view', ['site' => $site->parsing_name,'type' => 'passed']) }}">
                                    passed
                                </a> reports - <strong>{{ count($site->passedReports) }}</strong></p>
                            <p>
                                <a class="btn btn-success" href="{{ route('admin_sites_update', ['site' => $site->parsing_name]) }}">
                                    <i class="icon-pencil"></i> Edit Info
                                </a>
                            </p>
                            <p>Current site has {{ count($site->descriptions) }} description(s)<br>
                                <a class="btn btn-success"
                                   href="{{ route('admin_descriptions_view', ['type' => 'site', 'name' => $site->parsing_name]) }}">
                                    <i class="icon-pencil"></i> Add/Edit Description
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection