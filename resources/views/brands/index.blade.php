@extends('layouts.app')

@section('title', trans('brands.phrase.index', ['number' => count($brands)]))

@section('content')
    <div class="container">
        <div class="row">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('header.home')</a>
                </li>
                <li class="active text-capitalize"><strong>@lang('header.brand')</strong></li>
            </ul>
            <div class="col-md-10 col-md-offset-1 text-center">
                <h4>@lang('brands.phrase.index', ['number' => count($brands)])</h4><br>
            </div>
            @foreach($brands as $brand)
                <a class="link-unstyled" href="{{ route('brand_view', ['brand' => $brand->url_name]) }}">
                    <div class="col-lg-2 col-sm-3 col-xs-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">{{ $brand->name }}</div>
                            <div class="panel-body">
                                @if ($brand->image)
                                    <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                         src="{{ asset('storage/images/logos/brands') }}/{{$brand->image->name}}">
                                @else
                                    <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                         src="{{ asset('images/no-image.jpeg') }}">
                                @endif
                                <div class="text-center small">
                                    @if(count($brand->products) > 1)
                                        <strong>@lang('brands.products.count', ['count' => count($brand->products)])</strong>
                                    @elseif(count($brand->products) === 1)
                                        <strong>@lang('brands.products.count.single')</strong>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection