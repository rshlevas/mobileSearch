
    @foreach($products as $product)
        @php $comparePhrase = $product->profiles_count > 1 ?
            trans('products.compare.prices.several', ['number' => $product->profiles_count]) :
            trans('products.compare.prices.single');
        @endphp
        <div class="col-md-12">
            <a class="link-unstyled"
               title="{{ $comparePhrase }}"
               href="{{ route('product_view', ['url_name' => $product->url_name]) }}">
                <div class="col-md-3">
                    @if ($product->imageThumbnail)
                        <img style="height: 80px;" class="img-responsive center-block"
                             src="{{ asset($product->imageThumbnail->path) }}">
                    @elseif ($product->image)
                        <img style="height: 80px;" class="img-responsive center-block"
                             src="{{ asset($product->image->path) }}">
                    @else
                        <img style="height: 80px;" class="img-responsive center-block" src="{{ asset('images/no-image.jpeg') }}">
                    @endif
                </div>
                <div class="col-md-3">
                    <h4 class="text-center">
                        <i class="icon-{{ strtolower($product->best_price->currency) }}"></i><br>
                        {{ $product->best_price->price }}
                    </h4>
                </div>
                <div class="col-md-6">
                    <span class="align-middle">{{$product->name}}</span><br>
                    <span class="small text-center">
                        <strong>
                            <u>{{ $comparePhrase }}</u>
                        </strong>
                    </span>
                </div>
            </a>
        </div>

    @endforeach
    <div class="col-lg-1 col-lg-offset-8 col-sm-3 col-sm-offset-5 col-xs-4 col-xs-offset-3">
        <a href="{{ $route }}" class="btn btn-primary">@lang('buttons.view.more')  <i class="icon-double-angle-right"></i></a>
    </div>