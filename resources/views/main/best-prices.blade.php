<div id="best-prices" class="row">
    <div class="col-sm-12">
        <h3 class="text-center text-uppercase">@lang('homepage.best.prices')</h3><br>
        @foreach($bestPrices as $chunk)
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 panel panel-default">
                <div class="panel-body">
                    @foreach($chunk as $product)
                        <div class="col-md-12">
                            <a class="link-unstyled" title="{{ $product->name }}"
                               href="{{ route('product_view', ['url_name' => $product->url_name]) }}">
                                <div class="col-md-3">
                                    @if ($product->imageThumbnail)
                                        <img style="height: 80px;" class="img-responsive center-block"
                                             src="{{ asset($product->imageThumbnail->path) }}">
                                    @elseif ($product->image)
                                        <img style="height: 80px;" class="img-responsive center-block"
                                             src="{{ asset($product->image->path) }}">
                                    @else
                                        <img style="height: 80px;" class="img-responsive center-block"
                                             src="{{ asset('images/no-image.jpeg') }}">
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <h4 class="text-center">
                                        <i class="icon-{{ strtolower($product->best_price->currency) }}"></i>
                                        {{ $product->best_price->price }}
                                    </h4>
                                </div>
                                <div class="col-md-6">
                                    <h5><strong>{{ $product->brand->name }}</strong></h5>
                                    <span class="align-middle small">{{ $product->short_model }}</span><br>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>