<div id="sites" class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <h3 class="text-center text-uppercase">@lang('homepage.partners')</h3><br>
        @foreach($sites as $site)
            <a href="{{ route('recyclers_view', ['site' => $site->parsing_name]) }}">
                <div class="col-lg-3 col-sm-4 col-xs-6">
                    <div class="panel panel-primary" style="height: 110px;">
                        <div class="panel-body">
                            @if ($site->image)
                                <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                     src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}">
                            @else
                                <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                     src="{{ asset('images/no-image.jpeg') }}">
                            @endif
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
        @if ( count($sites) == 12)
            <div class="col-lg-1 col-lg-offset-10 col-sm-3 col-sm-offset-8 col-xs-4 col-xs-offset-6">
                <a href="{{ route('recyclers_list') }}" class="btn btn-primary">@lang('buttons.view.more')  <i class="icon-double-angle-right"></i></a>
            </div>
        @endif
    </div>
</div>