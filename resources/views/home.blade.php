<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div id="app">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
            <div class="collapse navbar-collapse text-uppercase" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#about">@lang('header.works')</a></li>
                    <li><a href="#most_popular">@lang('header.most.popular')</a></li>
                    <li><a href="#iphone">Iphone</a></li>
                    <li><a href="#samsung">Samsung Galaxy</a></li>
                    <li><a href="#best-prices">@lang('header.prices')</a></li>
                    <li><a href="#sites">@lang('header.partners')</a></li>
                    <li><a href="#brands">@lang('header.brand')</a></li>
                    <li class="dropdown text-uppercase">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-flag"></i> {{ Config::get('languages')[App::getLocale()] }}
                        </a>
                        <ul class="dropdown-menu">
                            @foreach (Config::get('languages') as $lang => $language)
                                @if ($lang != App::getLocale())
                                    <li>
                                        <i class="flag-icon flag-icon-us"></i><a href="{{ route('lang_switch', $lang) }}">{{$language}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="jumbotron text-center">
        <h1>Header</h1>
        <p>We specialize in phones</p>
        <form class="form-inline" method="POST" action="{{ route('search') }}">
            {{ csrf_field() }}
            <div class="input-group">
                <input id="search" type="text" class="form-control input-lg" name="search" size="50"
                       placeholder="@lang('header.search.placeholder')" required>
                @if ($errors->has('search'))
                    <span class="help-block">
                            <strong>{{ $errors->first('search') }}</strong>
                        </span>
                @endif
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-info btn-lg text-uppercase">@lang('buttons.search')</button>
                </div>
            </div>
        </form>
    </div>

    <div class="container">
        <div class="row">
            @include('main.how-it-works');
        </div>
        <br>
        <div class="row">
            <div id="most_popular" class="col-sm-4 col-xs-12">
                <h3 class="text-center text-uppercase">@lang('homepage.most.popular')</h3><br>
                    @include('main.most-popular', [
                        'products' => $mostPopular,
                        'route' => route('most_popular'),
                    ])
            </div>
            <div id="iphone" class="col-sm-4 col-xs-12">
                <h3 class="text-center text-uppercase">Iphones</h3><br>
                    @include('main.most-popular', [
                        'products' => $iphones,
                        'route' => route('key_product_view', ['keyPhone' => 'iphone']),
                    ])
            </div>
            <div id="samsung" class="col-sm-4 col-xs-12">
                <h3 class="text-center text-uppercase">Samsung Galaxy</h3><br>
                    @include('main.most-popular', [
                        'products' => $samsung,
                        'route' => route('key_product_view', ['keyPhone' => 'galaxy']),
                    ])
            </div>
        </div>
        <br>
            @include('main.best-prices', ['bestPrices' => $bestPrices])
        <br>
            @include('main.sites', ['sites' => $sites])
        <br>
            @include('main.brands', ['brands' => $brands])
        <br>




    </div>
</div>

<footer class="container text-center">
    <h3>FOOTER 2017</h3>
    <div class="col-md-12 row">
        @foreach($posts as $post)
            <div class="col-md-4">
                <a class="link-unstyled" href="{{ $post->guid }}"><h3>{{ $post->post_title }}</h3></a>
                {{ $post->post_modified }}
            </div>
        @endforeach
        <br>
    </div>
    <div class="col-md-12">
        Some other info
    </div>
    <br>
    <br>

</footer>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


    <script>
        /*$('#search').autocomplete({
            source: function(request, response){

                $.ajax({
                    url: "",
                    type: "POST",
                    dataType: "json",
                    data:{ search: request.term },
                    success: function(data){
                        response($.map(data, function(item){
                            return item;
                        }));
                    }
                });
            },
            minLength: 2
        });*/


        $(document).ready(function(){
            $("#search").autocomplete({
                source: function(request, response){
                    $.ajax({
                        url: "{{ route('search_autocomplete') }}",
                        type: "POST",
                        dataType: "json",
                        data:{ search: request.term },
                        success: function(data){
                            response($.map(data, function(item){
                                return item;
                            }));
                        }
                    });
                },
                focus: function( event, ui ) {
                    //$( "#search" ).val( ui.item.name );
                    return false;
                },
                select: function( event, ui ) {
                    window.location.href = ui.item.url;
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                var inner_html;
                if (item.message) {
                    inner_html = '<div class="col-md-8 col-sm-offset-2 small">' +
                        item.message + '</div>';
                } else {
                    inner_html = '<a class="link-unstyled" href="' + item.url +
                        '" title="' + item.title + '">' +
                        '<div class="col-md-3"><img style="height: 60px;"' +
                        ' class="img-responsive center-block" src="' + item.image +
                        '" ></div><div class="col-md-2"><h4 class="text-center small"><b>' +
                        '<i class="icon-' + item.currency + '"></i> ' + item.price +
                        '</b></h4></div><div class="col-md-6 text-center small">' +
                        item.name + '<br><u class="small">' + item.compare +
                        '</u></div></a>';
                }
                return $( "<li class=\"ui-menu-item\"></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
            };
        });


    </script>
    <script>
        $(document).ready(function(){
            // Add smooth scrolling to all links in navbar + footer link
            $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function(){

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });

            $(window).scroll(function() {
                $(".slideanim").each(function(){
                    var pos = $(this).offset().top;

                    var winTop = $(window).scrollTop();
                    if (pos < winTop + 600) {
                        $(this).addClass("slide");
                    }
                });
            });
        })
    </script>

</body>
</html>






