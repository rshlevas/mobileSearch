<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <div class="collapse navbar-collapse text-uppercase" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('about_view') }}">@lang('header.works')</a></li>
                        <li><a href="{{ route('most_popular') }}">@lang('header.most.popular')</a></li>
                        <li><a href="{{ route('key_product_view', ['keyPhone' => 'iphone']) }}">Iphone</a></li>
                        <li><a href="{{ route('key_product_view', ['keyPhone' => 'galaxy']) }}">Samsung Galaxy</a></li>
                        <li><a href="{{ route('best_prices_view') }}">@lang('header.prices')</a></li>
                        <li><a href="{{ route('recyclers_list') }}">@lang('header.partners')</a></li>
                        <li><a href="{{ route('brand_main') }}">@lang('header.brand')</a></li>
                        <li class="dropdown text-uppercase">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-flag"></i> {{ Config::get('languages')[App::getLocale()] }}
                            </a>
                            <ul class="dropdown-menu">
                                @foreach (Config::get('languages') as $lang => $language)
                                    @if ($lang != App::getLocale())
                                        <li>
                                            <a href="{{ route('lang_switch', $lang) }}">{{$language}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="jumbotron text-center">
            <h1>Header</h1>
            <p>We specialize in phones</p>
            <form class="form-inline" method="POST" action="{{ route('search') }}">
                {{ csrf_field() }}
                <div class="input-group">
                    <input id="search" type="text" class="form-control input-lg" name="search" size="50"
                           placeholder="@lang('header.search.placeholder')" required>
                    @if ($errors->has('search'))
                        <span class="help-block">
                            <strong>{{ $errors->first('search') }}</strong>
                        </span>
                    @endif
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-lg text-uppercase">@lang('buttons.search')</button>
                    </div>
                </div>
            </form>
        </div>

        @yield('content')
    </div>
    <footer class="container text-center">
        <h3>FOOTER 2017</h3>
        <div class="col-md-12 row">
            @foreach($posts as $post)
            <div class="col-md-4">
                <a class="link-unstyled" href="{{ $post->guid }}">
                    <h3>{{ $post->post_title }}</h3></a>
                    {{ $post->post_modified }}
            </div>
            @endforeach
                <br>
        </div>
        <div class="col-md-12">
            Some other info
        </div>
        <br>
        <br>

    </footer>
    <!-- Scripts -->

    <script>
      /*$('#search').autocomplete({
            source: function(request, response){
                $.ajax({
                    url: "",
                    type: "POST",
                    dataType: "json",
                    data:{ search: request.term },
                    success: function(data){
                        response($.map(data, function(item){
                            return item;
                        }));
                    }
                });
            },
            minLength: 2
        });*/
        $(document).ready(function(){
            $("#search").autocomplete({
                source: function(request, response){
                    $.ajax({
                        url: "{{ route('search_autocomplete') }}",
                        type: "POST",
                        dataType: "json",
                        data:{ search: request.term },
                        success: function(data){
                            response($.map(data, function(item){
                                return item;
                            }));
                        }
                    });
                },
                focus: function( event, ui ) {
                    //$( "#search" ).val( ui.item.name );
                    return false;
                },
                select: function( event, ui ) {
                    window.location.href = ui.item.url;
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                var inner_html;
                if (item.message) {
                    inner_html = '<div class="col-md-8 col-sm-offset-2 small">' +
                        item.message + '</div>';
                } else {
                    inner_html = '<a class="link-unstyled" href="' + item.url +
                        '" title="' + item.title + '">' +
                        '<div class="col-md-3"><img style="height: 60px;"' +
                        ' class="img-responsive center-block" src="' + item.image +
                        '" ></div><div class="col-md-2"><h4 class="text-center small"><b>' +
                        '<i class="icon-' + item.currency + '"></i> ' + item.price +
                        '</b></h4></div><div class="col-md-6 text-center small">' +
                        item.name + '<br><u class="small">' + item.compare +
                        '</u></div></a>';
                }
                return $( "<li class=\"ui-menu-item\"></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
            };
        });
    </script>
    <script>
        function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("productTable");
            switching = true;
            dir = "asc";
            while (switching) {
                switching = false;
                rows = table.getElementsByTagName("TR");
                for (i = 1; i < (rows.length - 1); i++) {
                    shouldSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    if (dir == "asc") {
                        if (parseFloat(x.getAttribute('data-number')) > parseFloat(y.getAttribute('data-number'))) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    } else if (dir == "desc") {
                        if (parseFloat(x.getAttribute('data-number')) < parseFloat(y.getAttribute('data-number'))) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    switchcount ++;
                } else {
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }
    </script>

</body>
</html>


