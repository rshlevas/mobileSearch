@extends('layouts.app')

@section('title', trans('products.title', ['product' => $product->name, 'price' => $product->best_price->price]))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li>
                        <a href="{{ route('brand_view', ['brand' => $product->brand->url_name]) }}">{{ $product->brand->name }}</a>
                    </li>
                    <li class="active text-capitalize"><strong>{{ $product->name }}</strong></li>
                </ul>
            </div>
            <div class="col-md-12" id="product_info">
                <div class="col-md-8 col-xs-12">
                    @include('products.components.general_info', ['product' => $product])
                </div>
                <div class="col-md-4" id="best_price">
                    @if($bestPrice)
                        @include('products.components.best_price', ['price' => $bestPrice])
                    @endif
                </div>
                <div class="row">
                    @include('products.components.filters', ['product' => $product, 'filters' => $filters])
                </div>
                <br>
                <div class="row" id="prices">
                    @include('products.components.product_site_table', ['prices' => $prices, 'bestPrice' => $bestPrice, 'unknownSites' => $unknownSites])
                </div>
            </div>

        </div>


    </div>

    <script>
        $('.selectpicker').selectpicker();

        $('.filters').change(function() {
            var url = window.location.href + '/filters';
            var condition = $('#condition').val();
            var network = $('#network').val();

            $.setParamsToUrl = function (network, condition) {
                var params =  {};
                params['condition'] = condition;
                params['network'] = network;

                return $.param(params);
            };
            var str = $.setParamsToUrl(network, condition);
            if (str) {
                var bestPriceEndPoint = url + '/best-price?' + str;
                var pricesEndPoint = url + '/prices?' + str;
            } else {
                return false;
            }

            $.ajax({
                type: 'get',
                url: bestPriceEndPoint ,
                success: function (data) {
                    $("#best_price").html(data.html);
                }
            });
            $.ajax({
                type: 'get',
                url: pricesEndPoint ,
                success: function (data) {
                    $("#prices").html(data.html);
                }
            });
            return false;
        })
    </script>
@endsection