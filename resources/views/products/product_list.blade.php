@foreach($products as $product)
    <a class="link-unstyled" href="{{ route('product_view', ['product' => $product->url_name]) }}"
        title="{{ $product->name }}">
        <div class="col-lg-2 col-sm-3 col-xs-4">
            <div class="panel panel-primary" style="height: 280px;">
                <div class="panel-heading">{{ $product->brand->name }}</div>
                <div class="panel-body">
                    @if ($product->imageThumbnail)
                        <img style="height: 80px;" class="thumbnail img-responsive center-block"
                             src="{{ asset($product->imageThumbnail->path) }}">
                    @elseif ($product->image)
                        <img style="height: 80px;" class="thumbnail img-responsive center-block"
                             src="{{ asset($product->image->path) }}">
                    @else
                        <img style="height: 80px;" class="thumbnail img-responsive center-block"
                             src="{{ asset('images/no-image.jpeg') }}">
                    @endif
                    <div class="text-center">{{ $product->short_model }}</div>
                        <div class="small text-center">
                            <strong>
                                <u>
                                    @if($product->profiles_count > 1)
                                        @lang('products.compare.prices.several', ['number' => $product->profiles_count])
                                    @else
                                        @lang('products.compare.prices.single')
                                    @endif
                                </u>
                            </strong>
                        </div>
                    <div class="text-center">
                        <h4>
                            <i class="icon-{{ strtolower($product->best_price->currency) }}"></i>
                            {{ $product->best_price->price }}
                        </h4>
                    </div>
                </div>

            </div>
        </div>
    </a>
@endforeach
<div class="col-md-10 col-md-offset-1 text-center">
    <h4>{{ $products->links() }}</h4><br>
</div>