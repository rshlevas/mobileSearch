<tr>
    <td>
        <a href="{{ route('recyclers_view', ['site' => $price->productProfile->site->parsing_name]) }}">
            @if ($price->productProfile->site->image)
                <img style="max-height: 120px;"
                     src="{{ asset('storage/images/logos/sites') }}/{{$price->productProfile->site->image->name}}"
                     class="img-responsive img-thumbnail">
            @else
                <img style="max-height: 120px;" src="{{ asset('images/no-image.jpeg') }}"
                     class="img img-thumbnail">
            @endif
        </a>
    </td>
    @if ($price->productProfile->site->trust_point and $price->productProfile->site->trust_point != 0.00)
    <td style="vertical-align: middle;" data-number="{{ $price->productProfile->site->stars_number }}">
            <a href="{{ $price->productProfile->site->trustpilot_link }}">
                <img src="{{ asset("images/{$price->productProfile->site->stars_number}-stars.png") }}"
                     class="img img-thumbnail img-responsive">
            </a>
            <p class="small"><i class="icon-li icon-ok" style="color:green"></i> Trustpilot</p>
    </td>
    @else
    <td style="vertical-align: middle;" data-number="0">
            <i class="icon-fixed-width icon-ban-circle"
               style="color:red"></i> @lang('recyclers.no.rating')
    </td>
    @endif
    <td style="vertical-align: middle;" data-number="{{ $price->price }}">
        <h3>
            <i class="icon-{{ strtolower($price->currency) }}"></i> {{ $price->price }}
        </h3>
    </td>
    @if($price->productProfile->site->paymentTerm)
    <td style="vertical-align: middle;" data-number="{{ $price->productProfile->site->paymentTerm->id }}">
        {{ $price->productProfile->site->paymentTerm->name }}</td>
    @else
    <td style="vertical-align: middle;" data-number="0">
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
    </td>
    @endif
    <td style="vertical-align: middle;">
        @if( count($price->productProfile->site->payments) > 0)
            @foreach($price->productProfile->site->payments as $payment)
                {{ $payment->name }}<br>
            @endforeach
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td style="vertical-align: middle;" data-number="{{ $price->productProfile->site->is_returnable }}">
        @if ($price->productProfile->site->is_returnable == 1)
            <i class="icon-li icon-ok" style="color:green"></i>
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td style="vertical-align: middle;" data-number="{{ $price->productProfile->site->is_free_post }}">
        @if ($price->productProfile->site->is_free_post == 1)
            <i class="icon-li icon-ok" style="color:green"></i>
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td style="vertical-align: middle;"><a class="btn btn-{{ $btn }}"
        href="{{ route('sell_link', ['link' => $price->productProfile->link->id]) }}">@lang('buttons.sell')</a>
    </td>
</tr>