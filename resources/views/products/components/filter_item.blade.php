<div class="col-md-12" id="{{"{$filterName}s"}}">
    <div class="panel-body">
        <div class="form-group">
            <label for="{{ $filterName }}" class="col-md-1 control-label">@lang("products.filter.$filterName"):</label>
            <div class="col-md-2">
                @if(count($filters) <= 1)
                    <select name="{{ $filterName }}" class="form-control selectpicker filters" data-style="btn-default"
                            id="{{ $filterName }}" disabled>
                        @foreach($filters as $filter)
                            <option value="{{ $filter['alias'] }}">
                                {{ $filter['name'] }}
                            </option>
                        @endforeach
                    </select>
                @else
                    <select name="{{ $filterName }}" class="form-control selectpicker filters" id="{{ $filterName }}"
                            data-style="btn-primary">
                        @foreach($filters as $filter)
                            <option value="{{ $filter['alias'] }}">
                                {{ $filter['name'] }}
                            </option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>
    </div>
</div>