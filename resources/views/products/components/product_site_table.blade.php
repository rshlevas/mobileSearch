@if ($bestPrice)
    <table class="table table-hover text-center">
        <thead class="text-uppercase">
        <tr>
            <th class="col-sm-2"></th>
            <th class="col-sm-1"></th>
            <th class="col-sm-2"></th>
            <th class="col-sm-2"></th>
            <th class="col-sm-2"></th>
            <th class="col-sm-1"></th>
            <th class="col-sm-1"></th>
            <th class="col-sm-1"></th>
        </tr>
        <tr>
            <th class="text-center success" colspan="8">
                <h4>
                    <strong>@lang('products.top.price.table.header', ['product' => $bestPrice->productProfile->product->name])</strong>
                </h4>
            </th>
        </tr>
        </thead>
        <tbody>
            @include('products.components.product_table_content', ['price' => $bestPrice, 'btn' => 'success'])
        </tbody>
    </table>
@endif
<table id="productTable" class="table table-hover text-center">
    <thead class="text-uppercase">
    <tr>
        <th class="col-sm-2 info text-center">@lang('products.site')</th>
        <th class="col-sm-1 info text-center">@lang('products.site.trust')
            <i onclick="sortTable(1)" class="icon-fixed-width icon-sort"></i>
        </th>
        <th class="col-sm-2 info text-center">@lang('products.price')
            <i onclick="sortTable(2)" class="icon-fixed-width icon-sort"></i>
        </th>
        <th class="col-sm-2 info text-center">@lang('recyclers.term')
            <i onclick="sortTable(3)" class="icon-fixed-width icon-sort"></i>
        </th>
        <th class="col-sm-2 info text-center">@lang('recyclers.method')</th>
        <th class="col-sm-1 info text-center">@lang('recyclers.return')
            <i onclick="sortTable(5)" class="icon-fixed-width icon-sort"></i>
        </th>
        <th class="col-sm-1 info text-center">@lang('recyclers.free.post')
            <i onclick="sortTable(6)" class="icon-fixed-width icon-sort"></i>
        </th>
        <th class="col-sm-1 info text-center">@lang('products.click.sell')</th>
    </tr>
    </thead>
    <tbody>
    @if (count($prices))
        @foreach($prices as $price)
            @include('products.components.product_table_content', ['price' => $price, 'btn' => 'primary'])
        @endforeach
    @elseif (! count($prices) && ! $bestPrice)
        <tr>
            <td colspan="8"><h4>@lang('products.no-prices')</h4></td>
        </tr>
    @endif
    @if (count($unknownSites))
        @include('products.components.product_table_unknown', ['sites' => $unknownSites])
    @endif
    </tbody>
</table>