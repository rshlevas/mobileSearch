<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="text-center">@lang('products.top.price')</h3></div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="col-md-12">
                <a href="{{ route('recyclers_view', ['site' => $bestPrice->productProfile->site->parsing_name]) }}">
                    @if ($bestPrice->productProfile->site->image)
                        <img style="height: 80px;"
                             src="{{ asset('storage/images/logos/sites') }}/{{$bestPrice->productProfile->site->image->name}}"
                             class="img-responsive">
                    @else
                        <img style="height: 80px;" src="{{ asset('images/no-image.jpeg') }}"
                             class="img img-responsive">
                    @endif
                </a>
            </div>
            <div class="col-md-12">
                @if ($bestPrice->productProfile->site->trust_point and $bestPrice->productProfile->site->trust_point != 0.00)
                    <a href="{{ $bestPrice->productProfile->site->trustpilot_link }}">
                        <img src="{{ asset("images/{$bestPrice->productProfile->site->stars_number}-stars.png") }}"
                             class="img img-thumbnail img-responsive">
                    </a>
                    <p class="small text-center"><i class="icon-li icon-ok" style="color:green"></i> Trustpilot</p>
                @else
                    <i class="icon-fixed-width icon-ban-circle"
                       style="color:red"></i> @lang('recyclers.no.rating')
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <h3><i class="icon-{{ strtolower($bestPrice->currency) }}"></i> {{ $bestPrice->price }}</h3>
            </div>
            <div class="col-md-12">
                <a class="btn btn-primary"
                    href="{{ route('sell_link', ['link' => $bestPrice->productProfile->link->id]) }}">
                    @lang('buttons.sell.now')
                </a>
            </div>
        </div>
    </div>
</div>
