@foreach($sites as $site)
    <tr>
        <td>
            <a href="{{ route('recyclers_view', ['site' => $site->parsing_name]) }}">
                @if ($site->image)
                    <img style="max-height: 120px;"
                         src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}"
                         class="img-responsive img-thumbnail">
                @else
                    <img style="max-height: 120px;" src="{{ asset('images/no-image.jpeg') }}"
                         class="img img-thumbnail">
                @endif
            </a>
        </td>
        <td style="vertical-align: middle;">
            @if ($site->trust_point and $site->trust_point != 0.00)
                <a href="{{ $site->trustpilot_link }}">
                    <img src="{{ asset("images/{$site->stars_number}-stars.png") }}"
                         class="img img-thumbnail img-responsive">
                </a>
                <p class="small"><i class="icon-li icon-ok" style="color:green"></i> Trustpilot</p>
            @else
                <i class="icon-fixed-width icon-ban-circle"
                   style="color:red"></i> @lang('recyclers.no.rating')
            @endif
        </td>
        <td style="vertical-align: middle;"><i class="icon-info"> </i><strong>@lang('products.price.unavailable')</strong></td>
        <td style="vertical-align: middle;">
            @if($site->paymentTerm)
                {{ $site->paymentTerm->name }}
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if( count($site->payments) > 0)
                @foreach($site->payments as $payment)
                    {{ $payment->name }}<br>
                @endforeach
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if ($site->is_returnable == 1)
                <i class="icon-li icon-ok" style="color:green"></i>
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if ($site->is_free_post == 1)
                <i class="icon-li icon-ok" style="color:green"></i>
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;"><a class="btn btn-primary"
            href="{{ $site->path }}">@lang('buttons.check.site')</a>
        </td>
    </tr>
@endforeach