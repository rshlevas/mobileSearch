<div class="col-md-6 col-sm-6">
    @if ($product->image)
        <img class="img-responsive img-thumbnail" src="{{ asset($product->image->path) }}">
    @else
        <img class="img-responsive img-thumbnail" src="{{ asset('images/no-image.jpeg') }}">
    @endif
</div>
<div class="col-md-6 col-sm-6">
    <h3>@lang('products.model'): {{ $product->model }}</h3>
    <h5>@lang('brands.brand'): <a
                href="{{ route('brand_view', ['brand' => $product->brand->url_name]) }}">{{ $product->brand->name }}</a>
    </h5>
    <p><em>@lang('products.compare', ['product' => $product->name])</em></p>
    <p><a class="btn btn-primary" href="{{ route('quiz_main') }}">Check you phone condition</a></p>
</div>