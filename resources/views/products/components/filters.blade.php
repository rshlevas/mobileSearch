@foreach($filters as $filterName => $filterGroup)
    @include('products.components.filter_item', ['filters' => $filterGroup, '$filterName' => $filterName])
@endforeach

