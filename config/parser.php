<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sites
    |--------------------------------------------------------------------------
    |
    | Here you may specify the sites specific data, that will be used for seeding
    | during deployment. Sites should be separated to groups:
    |   'sites' => [
    |       'Group name' => [
    |           'Site name' => [
    |               'path' => 'link of the site',
    |               'api_link' => 'api link',
    |           ],
    |       ],
    |   ],
    |
    */
    'sites' => [
        'Webgains' => [
            'ee Recycle' => [
                'path' => 'https://recycle.ee.co.uk/',
                'parsing_name' => 'ee_recycle',
                'api_link' => 'http://www.webgains.com/affiliates/datafeed.html?action=download&campaign=21547&feeds=661&categories=1879,1904&fields=extended&fieldIds=category_id,category_name,category_path,deeplink,description,image_url,last_updated,merchant_category,price,product_id,product_name,program_id,program_name,brand,currency,image_thumbnail_url,image_url,model_number&format=csv&separator=comma&zipformat=none&stripNewlines=0&apikey=af3b56d066c290b2cdde6fa72b7d8152',
            ],
            '8 Mobile' => [
                'path' => 'http://www.8mobile.com/sell-recycle/',
                'parsing_name' => '8_mobile',
                'api_link' => 'http://www.webgains.com/affiliates/datafeed.html?action=download&campaign=21547&feeds=1599,244&categories=1879,1904&fields=extended&fieldIds=category_id,category_name,category_path,deeplink,description,image_url,last_updated,merchant_category,price,product_id,product_name,program_id,program_name,brand,currency,image_url,model_number,non_working_price&format=csv&separator=comma&zipformat=none&stripNewlines=0&apikey=af3b56d066c290b2cdde6fa72b7d8152',
            ],
            'Mobile Cash Mate' => [
                'path' => 'https://www.mobilecashmate.co.uk/',
                'parsing_name' => 'mobile_cash_mate',
                'api_link' => 'http://www.webgains.com/affiliates/datafeed.html?action=download&campaign=21547&feeds=1495&categories=1879,1904&fields=extended&fieldIds=category_id,category_name,category_path,deeplink,description,image_url,last_updated,merchant_category,price,product_id,product_name,program_id,program_name,currency,image_url&format=csv&separator=comma&zipformat=none&stripNewlines=0&apikey=af3b56d066c290b2cdde6fa72b7d8152'
            ],
            'Simply Drop' => [
                'path' => 'http://simplydrop.co.uk/',
                'parsing_name' => 'simply_drop',
                'api_link' => 'http://www.webgains.com/affiliates/datafeed.html?action=download&campaign=21547&feeds=1643&categories=1879,1908,20463,1904,8192,18876,21609,8948,8199,8200,8204,8196&fields=extended&fieldIds=category_id,category_name,category_path,deeplink,description,image_url,last_updated,merchant_category,price,product_id,product_name,program_id,program_name,best_sellers,brand,currency,image_url,model_number,non_working_price,normal_price,promotion_details,recommended_retail_price,short_description,star_sign&format=csv&separator=comma&zipformat=none&stripNewlines=0&apikey=af3b56d066c290b2cdde6fa72b7d8152'
            ],
            'Envirofone' => [
                'path' => 'https://www.envirofone.com/en-gb',
                'parsing_name' => 'envirofone',
                'api_link' => 'http://www.webgains.com/affiliates/datafeed.html?action=download&campaign=21547&feeds=244&categories=1879,1904,8948,8200,8204,8196&fields=extended&fieldIds=category_id,category_name,category_path,deeplink,description,image_url,last_updated,merchant_category,price,product_id,product_name,program_id,program_name,brand,currency,delivery_cost,delivery_period,embargo,expiry,Full_merchant_price,image_large_url,image_thumbnail_url,image_url,language,model_number,non_working_price,promotion_details,short_description,stock_level,stock_level_date&format=csv&separator=comma&zipformat=none&stripNewlines=0&apikey=af3b56d066c290b2cdde6fa72b7d8152'
            ],
        ],
        'AWIN' => [
            'Music Magple' => [
                'path' => 'http://www.musicmagpie.co.uk/',
                'parsing_name' => 'music_magpie',
                'api_link' => 'https://productdata.awin.com/datafeed/download/apikey/c53689dc703f2cf5f6cb1a91c23b5b5c/language/en/cid/348,354,350,351,352/fid/15849,15857/columns/aw_deep_link,product_name,aw_product_id,merchant_product_id,merchant_image_url,description,merchant_category,search_price,category_name,aw_image_url,currency,store_price,merchant_deep_link,last_updated,brand_name,colour,condition/format/csv/delimiter/%2C/compression/zip/adultcontent/1/'
            ],
            'O2 Recycle' => [
                'path' => 'https://www.o2recycle.co.uk/',
                'parsing_name' => 'o2_recycle',
                'api_link' => 'https://productdata.awin.com/datafeed/download/apikey/c53689dc703f2cf5f6cb1a91c23b5b5c/language/en/cid/348,354,350,351,352/fid/3234/columns/aw_deep_link,product_name,aw_product_id,merchant_product_id,merchant_image_url,description,merchant_category,search_price,merchant_name,merchant_id,category_name,category_id,aw_image_url,currency,store_price,delivery_cost,merchant_deep_link,language,last_updated,display_price,data_feed_id,brand_name,colour/format/csv/delimiter/%2C/compression/zip/adultcontent/1/'
            ],
        ],
        'Custom Parsers' => [
            'GeckoMobileRecycling' => [
                'path' => 'https://www.geckomobilerecycling.co.uk/',
                'parsing_name' => 'gecko_mobile',
                'api_link' => 'http://109.123.120.223/api/phones/1?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'CarphoneWarehouse'  => [
                'path' => 'https://www.carphonewarehouse.com/',
                'parsing_name' => 'carphone_warehouse',
                'api_link' => 'http://109.123.120.223/api/phones/2?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'Vodafone Trade In' => [
                'path' => 'https://tradein.vodafone.co.uk/',
                'parsing_name' => 'vodafone',
                'api_link' => 'http://109.123.120.223/api/phones/3?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'giffgaff' => [
                'path' => 'https://www.giffgaff.com/',
                'parsing_name' => 'giffgaff',
                'api_link' => 'http://109.123.120.223/api/phones/4?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'Macback' => [
                'path' => 'https://macback.co.uk/',
                'parsing_name' => 'macback',
                'api_link' => 'http://109.123.120.223/api/phones/5?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'Top Dollar Mobile' => [
                'path' => 'http://www.topdollarmobile.co.uk/',
                'parsing_name' => 'top_dollar_mobile',
                'api_link' => 'http://109.123.120.223/api/phones/6?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'Fonebank' => [
                'path' => 'http://www.fonebank.com/',
                'parsing_name' => 'fonebank',
                'api_link' => 'http://109.123.120.223/api/phones/7?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
            'Tesco Mobile' => [
                'path' => 'https://tradein.tescomobile.com/',
                'parsing_name' => 'tesco_mobile',
                'api_link' => 'http://109.123.120.223/api/phones/8?api_key=DSh3RmyfMq3Z6k8eG3nWzhZ5c'
            ],
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Default Queue Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of queues you want parsers to be pushed in.
    |
    */
    'queue' => 'parsing',

    /*
    |--------------------------------------------------------------------------
    | Default Job Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which job should be used for pushing parsers
    | into queue.
    |
    */
    'job' => \App\Jobs\ParseJob::class,

    /*
    |--------------------------------------------------------------------------
    | Site Name - Parser Class Relations
    |--------------------------------------------------------------------------
    |
    | Here you may specify which parser class should be used for site. You should
    | use correct site name, that described at the database, and full parser class
    | name with all namespaces:
    | 'parsers' => [
    |     'site_name' => '\Some\Name\Space\ExampleParser::class'
    ]
    |
    */
    'parsers' => [
        'fonebank' => \App\Parsers\JSONParser\CustomParsers\FonebankParser::class,
        'tesco_mobile' => \App\Parsers\JSONParser\CustomParsers\TescoMobileParser::class,
        'top_dollar_mobile' => \App\Parsers\JSONParser\CustomParsers\TopDollarMobileParser::class,
        'macback' => \App\Parsers\JSONParser\CustomParsers\MackbackParser::class,
        'giffgaff' => \App\Parsers\JSONParser\CustomParsers\GiffgaffParser::class,
        'vodafone' => \App\Parsers\JSONParser\CustomParsers\VodafoneParser::class,
        'carphone_warehouse' => \App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser::class,
        'gecko_mobile' => \App\Parsers\JSONParser\CustomParsers\GeckoMobileParser::class,
        'music_magpie' => \App\Parsers\CSVParser\AWINParser\MusicMagpieParser::class,
        'o2_recycle' => \App\Parsers\CSVParser\AWINParser\O2RecycleParser::class,
        'simply_drop' => \App\Parsers\CSVParser\WebgainsParser\SimplyDropParser::class,
        'mobile_cash_mate' => \App\Parsers\CSVParser\WebgainsParser\MobileCashParser::class,
        'ee_recycle' => \App\Parsers\CSVParser\WebgainsParser\RecycleParser::class,
        '8_mobile' => \App\Parsers\CSVParser\WebgainsParser\EightMobileParser::class,
        'envirofone' => \App\Parsers\CSVParser\WebgainsParser\EnvirofoneParser::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Services Configs
    |--------------------------------------------------------------------------
    |
    | Here you may specify the configs of services related to the parsing process
    |
    |
    */
    'services' => [
        /*
        |--------------------------------------------------------------------------
        | ModelService Configs
        |--------------------------------------------------------------------------
        |
        | Here you may specify the logic of model service. Here you can display the
        | order of Entities with their aliases, at which they should be checked and
        | then added at DB. The structure of this section should be such:
        |    \Some\Name\Space\ModelService::class => [
        |           'alias' => \Some\Name\Space\Model::class,
        |        ],
        ]
        */
        \App\Parsers\Services\ModelService::class => [
            'brand' => \App\Models\Brand::class,
            'product' => \App\Models\Product::class,
            'productProfile' => \App\Models\ProductProfile::class,
            'price' => \App\Models\ProductPrice::class,
            'link' => \App\Models\SellLink::class,
        ],
    ],
];