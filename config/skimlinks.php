<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Skimlinks Api Key
    |--------------------------------------------------------------------------
    |
    | This value is the api key for skimlinks.com api. This value is used when the
    | outgoing links need to be transformed for Skimlinks service usage.
    |
    */
    'key' => env('SKIMLINKS_SECRET'),

    /*
    |--------------------------------------------------------------------------
    | Skimlinks Domain
    |--------------------------------------------------------------------------
    |
    | This value is the domain, that registered for Skimlinks service.
    |
    */
    'domain' => env('SKIMLINKS_SUBDOMAIN'),

    /*
    |--------------------------------------------------------------------------
    | Skimlinks XS
    |--------------------------------------------------------------------------
    |
    | This value is used for link transformation as part of it.
    |
    */
    'xs' => env('SKIMLINKS_XS', 1),

];