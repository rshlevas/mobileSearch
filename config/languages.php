<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Languages
     |--------------------------------------------------------------------------
     |
     | Here you should declare available languages for your application
     |
     |
     */
    'en' => 'English',
    'de' => 'Deutsch',
];