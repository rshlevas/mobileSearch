<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Filters for filter service
    |--------------------------------------------------------------------------
    |
    | Here you may specify the how filters will displays for filter service
    |   'service' => [
    |       'filter name' => [
    |           'column in db' => 'column name',
    |           'default' => 1,
    |           'alias' => 'db value'
    |       ],
    |   ],
    |
    */

    'services' => [
        \App\Services\FilterPriceService::class => [
            'condition' => [
                'column' => 'condition',
                'new' => 4,
                'damaged' => 3,
                'not_working' => 2,
                'default' => 1,
            ],
            'network' => [
                'column' => 'network',
                'bt' => 13,
                'sky' => 12,
                'talkee' => 11,
                'giffgaff' => 10,
                'three' => 9,
                't-mobile' => 8,
                'virgin' => 7,
                'ee' => 6,
                'orange' => 5,
                'tesco' => 4,
                'vodafone' => 3,
                'o2' => 2,
                'default' => 1,
            ],
        ],
        \App\Services\FilterSelector::class => [
            'condition' => [
                'column' => 'condition',
                'filters' => [
                    4 => [
                        'alias' => 'new',
                        'name' => 'New',
                    ],
                    3 => [
                        'alias' => 'damaged',
                        'name' => 'Damaged',
                    ],
                    2 => [
                        'alias' => 'not_working',
                        'name' => 'Not Working',
                    ],
                    1 => [
                        'alias' => 'default',
                        'name' => 'Working',
                    ],
                ],
            ],
            'network' => [
                'column' => 'network',
                'filters' => [
                    13 => [
                        'alias' => 'bt',
                        'name' => 'BT Mobile',
                    ],
                    12 => [
                        'alias' => 'sky',
                        'name' => 'Sky Mobile',
                    ],
                    11 => [
                        'alias' => 'talkee',
                        'name' => 'TalkEE',
                    ],
                    10 => [
                        'alias' => 'giffgaff',
                        'name' => 'Giffgaff',
                    ],
                    9 => [
                        'alias' => 'three',
                        'name' => 'Three',
                    ],
                    8 => [
                        'alias' => 't-mobile',
                        'name' => 'T-Mobile',
                    ],
                    7 => [
                        'alias' => 'virgin',
                        'name' => 'Virgin',
                    ],
                    6 => [
                        'alias' => 'ee',
                        'name' => 'EE',
                    ],
                    5 => [
                        'alias' => 'orange',
                        'name' => 'Orange',
                    ],
                    4 => [
                        'alias' => 'tesco',
                        'name' => 'Tesco Mobile',
                    ],
                    3 => [
                        'alias' => 'vodafone',
                        'name' => 'Vodafone',
                    ],
                    2 => [
                        'alias' => 'o2',
                        'name' => 'O2',
                    ],
                    1 => [
                        'alias' => 'default',
                        'name' => 'Unlocked',
                    ],
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Filters info for DB seeding
    |--------------------------------------------------------------------------
    |
    | Here you may specify the filters info for DB seeding. It necessary for quiz
    |   'seeding' => [
    |       'filter name' => [
    |           'db-column' => 'Value'
    |       ],
    |   ],
    |
    */

    'seeding' => [
        'condition' => [
            'en' => [
                1 => [
                    'alias' => 'default',
                    'display' => 'Working',
                    'max_weight' => 49,
                    'min_weight' => 6,
                ],
                2 => [
                    'alias' => 'not_working',
                    'display' => 'Not Working',
                    'max_weight' => 100,
                    'min_weight' => 80,
                ],
                3 => [
                    'alias' => 'damaged',
                    'display' => 'Damaged',
                    'max_weight' => 79,
                    'min_weight' => 50,
                ],
                4 => [
                    'alias' => 'new',
                    'display' => 'New',
                    'max_weight' => 5,
                    'min_weight' => 0,
                ],
            ],
            'de' => [
                1 => [
                    'alias' => 'default',
                    'display' => 'Arbeiten',
                    'max_weight' => 49,
                    'min_weight' => 6,
                ],
                2 => [
                    'alias' => 'not_working',
                    'display' => 'Funktioniert Nicht',
                    'max_weight' => 100,
                    'min_weight' => 80,
                ],
                3 => [
                    'alias' => 'damaged',
                    'display' => 'Beschädigt',
                    'max_weight' => 79,
                    'min_weight' => 50,
                ],
                4 => [
                    'alias' => 'new',
                    'display' => 'Neu',
                    'max_weight' => 5,
                    'min_weight' => 0,
                ],
            ],
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Filters for view rendering
    |--------------------------------------------------------------------------
    |
    | Here you may specify how filters will displays during page rendering
    |   'view' => [
    |       'filter name' => [
    |           'alias' => 'Represent name'
    |       ],
    |   ],
    |
    */

    'product_display' => [
        'network' => [
            13 => 'BT Mobile',
            12 => 'Sky Mobile',
            11 => 'TalkEE',
            10 => 'Giffgaff',
            9 => 'Three',
            8 => 'T-Mobile',
            7 => 'Virgin',
            6 => 'EE',
            5 => 'Orange',
            4 => 'Tesco Mobile',
            3 => 'Vodafone',
            2 => 'O2',
            1 => 'Unlocked'
        ],
        'condition' => [
            1 => 'Working',
            2 => 'Not working',
            3 => 'Damaged',
            4 => 'New',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Filters for middleware
    |--------------------------------------------------------------------------
    |
    | Here you may define filters aliases for middleware checking
    |   'middleware' => [
    |       'filter name' => ['alias', 'alias']
    |   ],
    |
    */
    'middleware' => [
        'condition' => ['damaged', 'not_working', 'default', 'new'],
        'network' => [
            'three',
            't-mobile',
            'virgin',
            'ee',
            'orange',
            'tesco',
            'vodafone',
            'o2',
            'giffgaff',
            'bt',
            'sky',
            'talkee',
            'default'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Filters for parsing
    |--------------------------------------------------------------------------
    |
    | Here you may define filters aliases for parsers
    |   'parsing' => [
    |       'Parser Class' => [
    |           'filter' => [
    |              'parsing name' => 'db value'
    |           ],
    |       ],
    |   ],
    |
    */
    'parsing' => [
        \App\Parsers\CSVParser\AWINParser\MusicMagpieParser::class => [
            'condition' => [
                'Used' => 1,
            ],
            'network' => [
                '3 -' => 9,
                '-  3' => 9,
                'T-Mobile' => 8,
                'Virgin' => 7,
                'EE' => 6,
                'Orange' => 5,
                'Tesco' => 4,
                'Vodafone' => 3,
                'O2' => 2,
                'Unlocked' => 1,
            ],
        ],
        \App\Parsers\JSONParser\CustomParsers\CustomParser::class => [
            'condition' => [
                'Working' => 1,
                'Dead' => 2,
                'Faulty' => 3,
                'New' => 4,
            ],
            'network' => [
                'BT Mobile' => 13,
                'Sky Mobile' => 12,
                'TalkEE' => 11,
                'Giffgaff' => 10,
                '3 Mobile' => 9,
                'T-mobile' => 8,
                'Virgin' => 7,
                'EE' => 6,
                'Orange' => 5,
                'Tesco' => 4,
                'Vodafone' => 3,
                'O2' => 2,
                'Unlocked' => 1,
            ],
        ]
    ],

];