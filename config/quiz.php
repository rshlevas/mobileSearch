<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Quiz
    |--------------------------------------------------------------------------
    |
    | Here you may specify questions and their answers and order
    | for DB seeding during deployment.
    |   'questions' => [
    |       '0' => [
    |           'question' => [
    |               'content' => 'Question content',
    |               'lang' => 'en',
    |               'order' => 1,
    |           ],
    |           'answers' => [
    |               0 => [
    |                   'content' => 'Answer Content',
    |                   'weight' => 1,
    |               ],
    |           ]
    |       ]
    |   ],
    |
    */

    'questions' => [
        0 => [
            'question' => [
                'content' => 'How long you are using your phone?',
                'lang' => 'en',
                'order' => 2,
            ],
            'answers' => [
                0 => [
                    'content' => 'More then 2 months',
                    'weight' => 1,
                    'lang' => 'en'
                ],
                1 => [
                    'content' => 'Less then 2 months',
                    'weight' => 0,
                    'lang' => 'en'
                ],
            ]
        ],
        1 => [
            'question' => [
                'content' => 'Your phone model is new?',
                'lang' => 'en',
                'order' => 1,
            ],
            'answers' => [
                0 => [
                    'content' => 'Yes, It appeared less then half year ago',
                    'weight' => 0,
                    'lang' => 'en'
                ],
                1 => [
                    'content' => 'No, it appeared more then half year ago',
                    'weight' => 1,
                    'lang' => 'en'
                ],
            ]
        ],
        2 => [
            'question' => [
                'content' => 'Is your phone physically damaged?',
                'lang' => 'en',
                'order' => 3,
            ],
            'answers' => [
                0 => [
                    'content' => 'No, it\'s in ideal condition',
                    'weight' => 0,
                    'lang' => 'en'
                ],
                1 => [
                    'content' => 'No it\'s in great condition (light wear and tear is okay)',
                    'weight' => 1,
                    'lang' => 'en'
                ],
                2 => [
                    'content' => 'Yes (the screen is damaged, missing/broken or customised covers or buttons)',
                    'weight' => 2,
                    'lang' => 'en'
                ],
                3 => [
                    'content' => 'Yes, there\'s heavy damage to the device',
                    'weight' => 4,
                    'lang' => 'en'
                ],
            ]
        ],
        3 => [
            'question' => [
                'content' => 'Is your phone fully functional?',
                'lang' => 'en',
                'order' => 4,
            ],
            'answers' => [
                0 => [
                    'content' => 'Yes everything works',
                    'weight' => 0,
                    'lang' => 'en'
                ],
                1 => [
                    'content' => 'Yes it works but some buttons are missing or broken',
                    'weight' => 1,
                    'lang' => 'en'
                ],
                2 => [
                    'content' => 'No, the main menu/power button is faulty',
                    'weight' => 3,
                    'lang' => 'en'
                ],
                3 => [
                    'content' => 'No, it has software/hardware faults (it can\'t connect to a computer)',
                    'weight' => 2,
                    'lang' => 'en'
                ],
                4 => [
                    'content' => 'No, it doesn\'t power on/off or charge',
                    'weight' => 4,
                    'lang' => 'en'
                ],
            ]
        ],
        4 => [
            'question' => [
                'content' => 'Is your device water damaged?',
                'lang' => 'en',
                'order' => 5,
            ],
            'answers' => [
                0 => [
                    'content' => 'Yes, and it fails to power on',
                    'weight' => 4,
                    'lang' => 'en'
                ],
                1 => [
                    'content' => 'Yes, it\'s still fully functional',
                    'weight' => 2,
                    'lang' => 'en'
                ],
                2 => [
                    'content' => 'No, my device is not water damaged',
                    'weight' => 0,
                    'lang' => 'en'
                ],
            ]
        ],
    ]

];